<?php
include("./Layouts/loginControl.php");
include("../classes/MainClass.php");
include("../classes/uploadImage/class.upload.php");
ini_set("upload_tmp_dir", "/tmp");
$veritabani = new MainClass();
$country = $veritabani->getCountrys();

if (isset($_POST["islem"]) && $_POST["islem"] == "ekle") {
    $image;
    $handle = new \Verot\Upload\Upload($_FILES["profile"],'tr_TR');
        if ($handle->uploaded) {
            $handle->file_new_name_body   = time();
            $handle->image_resize         = true;
            $handle->image_x              = 100;
            $handle->image_ratio_y        = true;
            $handle->file_src_pathname    = $_FILES["profile"]["tmp_name"];
            $handle->process("../uploads/");
            if ($handle->processed) {
               $image =  "/uploads/".$handle->file_dst_name;
               
            } else {
              echo 'error : ' . $handle->error;
            }
          }
          
    echo $veritabani->addLocation($_POST["name"], $image, $_POST["country_id"],$_POST["local_inter"]);

}
if (isset($_POST["islem"]) && $_POST["islem"] == "update") {
    $image;
    $handle = new \Verot\Upload\Upload($_FILES["profile"],'tr_TR');
        if ($handle->uploaded) {
            $handle->file_new_name_body   = time();
            $handle->image_resize         = true;
            $handle->image_x              = 100;
            $handle->image_ratio_y        = true;
            $handle->file_src_pathname    = $_FILES["profile"]["tmp_name"];
            $handle->process("../uploads/");
            if ($handle->processed) {
               $image =  "/uploads/".$handle->file_dst_name;
               
            } else {
              echo 'error : ' . $handle->error;
            }
          }
        
   $avb =  $veritabani->updateLocation($_POST["name"], $image, $_POST["country_id"], $_POST["id"], $_POST["local_inter"]);
          print_r($avb);
}

?>

<?php include("./Layouts/header.php") ?>
<?php if (isset($_GET["update"])) {
    $client = $veritabani->getOneLocation($_GET["location_id"]);

?>
    <div class="page-content">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4> Location Productions Update </h4>
                        <form  enctype="multipart/form-data"  action="/admin/addLocation.php" method="post">


                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Locations Title</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="name" placeholder="Bluemind" value="<?php echo $client["location"] ?>" id=" example-text-input">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Location Country</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="country_id" id="">
                                        <?php for ($i=0; $i < count($country); $i++) { 
                                            
                                            echo '<option value="'.$country[$i]["country_id"].'">'.$country[$i]["country"].'</option>';

                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Local/International</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="local_inter" id="">
                                        <option value="1">INTERNATIONAL</option>
                                        <option value="0">LOCAL</option>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Location Photo </label>
                                <div class="col-md-8">
                                    <input type="file" name="profile" id="" class="form-control">

                                </div>
                            </div>


                            <div class="mb-3 row">
                                <div class="col-md-10">
                                    <input type="hidden" name="id" value="<?php echo $_GET["location_id"] ?>">
                                    <button class="btn btn-primary" type="submit" value="update" name="islem">Update</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="page-content">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4> Location Production Add </h4>
                        <form enctype="multipart/form-data"  action="/admin/addLocation.php" method="post">


                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Locations Title</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="name" placeholder="Ali"  id=" example-text-input">
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Location Country</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="country_id" id="">
                                        <?php for ($i=0; $i < count($country); $i++) { 
                                            
                                            echo '<option value="'.$country[$i]["country_id"].'">'.$country[$i]["country"].'</option>';

                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Local/International</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="local_inter" id="">
                                        <option value="1">INTERNATIONAL</option>
                                        <option value="0">LOCAL</option>
                                    </select>
                                </div>
                            </div>
                            
                          
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Lokasyon Image </label>
                                <div class="col-md-8">
                                    <input type="file" name="profile" id="" class="form-control">

                                </div>
                            </div>


                            <div class="mb-3 row">
                                <div class="col-md-10">
                                   
                                    <button class="btn btn-primary" type="submit" value="ekle" name="islem">Ekle</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php include("./Layouts/footer.php") ?>