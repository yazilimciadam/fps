<?php
include("./Layouts/loginControl.php");
include("../classes/MainClass.php");
include("../classes/uploadImage/class.upload.php");
ini_set("upload_tmp_dir", "/tmp");
$veritabani = new MainClass();
$country = $veritabani->getCountrys();
//var_dump($_POST);
if (isset($_POST["islem"]) && $_POST["islem"] == "ekle") {
   
          
    echo $veritabani->addContact($_POST["name"], $_POST["desc"], $_POST["address"], $_POST["phone"], $_POST["map"]);

}
if (isset($_POST["islem"]) && $_POST["islem"] == "update") {

          
    $veritabani->updateContact($_POST["name"], $_POST["desc"], $_POST["address"], $_POST["phone"], $_POST["map"], $_POST["id"]);
}

?>

<?php include("./Layouts/header.php") ?>
<?php if (isset($_GET["update"])) {
    $client = $veritabani->getContactOne($_GET["contact_id"]);

?>
    <div class="page-content">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4> Contact Location Update </h4>
                        <form   action="/admin/addContact.php" method="post">


                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Title</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="name" placeholder="Bluemind" value="<?php echo $client["title"] ?>" id=" example-text-input">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Address</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="address" placeholder="Address" value="<?php echo $client["address"] ?>" id=" example-text-input">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Description</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="desc" placeholder="Description" value="<?php echo $client["description"] ?>" id=" example-text-input">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Phone</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="phone" placeholder="Phone" value="<?php echo $client["phone"] ?>" id=" example-text-input">
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Map Link</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="map" placeholder="MAP" value="<?php echo $client["map"] ?>" id=" example-text-input">
                                </div>
                            </div>
                           
                           
                           
                         


                            <div class="mb-3 row">
                                <div class="col-md-10">
                                    <input type="hidden" name="id" value="<?php echo $_GET["contact_id"] ?>">
                                    <button class="btn btn-primary" type="submit" value="update" name="islem">Ekle</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="page-content">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4> Contact Location Add </h4>
                        <form  action="/admin/addContact.php" method="post">


                        <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Contact Title</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="name" placeholder="FPS İstanbul"  id=" example-text-input">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Contact Address</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="address" placeholder="Address" id=" example-text-input">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Contact Description</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="desc" placeholder="Description"  id=" example-text-input">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Contact Phone</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="phone" placeholder="Phone"  id=" example-text-input">
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Contact Map Link</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="map" placeholder="MAP" id=" example-text-input">
                                </div>
                            </div>
                           
                            


                            <div class="mb-3 row">
                                <div class="col-md-10">
                                   
                                    <button class="btn btn-primary" type="submit" value="ekle" name="islem">Ekle</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php include("./Layouts/footer.php") ?>