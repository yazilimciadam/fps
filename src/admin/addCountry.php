<?php 
include("./Layouts/loginControl.php");
include("../classes/MainClass.php");
include("../classes/uploadImage/class.upload.php");
ini_set("upload_tmp_dir", "/tmp");
$veritabani = new MainClass();



if(isset($_POST["islem"])){
  
    
    $files = array();
    foreach ($_FILES['file'] as $k => $l) {
      foreach ($l as $i => $v) {
      if (!array_key_exists($i, $files))
        $files[$i] = array();
        $files[$i][$k] = $v;
      }
    }

    foreach ($files as $file ) {
        
        $handle = new \Verot\Upload\Upload($file,'tr_TR');
        if ($handle->uploaded) {
            $handle->file_new_name_body   = time();
            $handle->image_resize         = true;
            $handle->image_x              = 100;
            $handle->image_ratio_y        = true;
            $handle->file_src_pathname    = $file["tmp_name"];
            $handle->process("../uploads/");
            if ($handle->processed) {
            $arr[] = array(
                "slide_class" => "slider",
                "image" => "/uploads/".$handle->file_dst_name,
            );
            var_dump($arr);
             
            } else {
              echo 'error : ' . $handle->error;
            }
          }
          unset($handle);
    }
    $images = json_encode($arr);
    $veritabani->AddCountry($images,$_POST["country_name"],$_POST["country_code"]);
   
}

?>

<?php include("./Layouts/header.php") ?>

                <div class="page-content">
                   
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4> Country Add </h4>
                                <form enctype="multipart/form-data"  action="/admin/addCountry.php" method="post">

                       
                                <div class="mb-3 row">
                                            <label for="example-text-input" class="col-md-2 col-form-label">Country  Name</label>
                                            <div class="col-md-10">
                                                <input class="form-control" type="text" name="country_name" placeholder="Türkiye"" id="example-text-input">
                                            </div>
                                        </div>
                                    <div class="mb-3 row">
                                            <label for="example-text-input" class="col-md-2 col-form-label">Country Code</label>
                                            <div class="col-md-10">
                                                <input class="form-control" type="text" placeholder="tr" name="country_code"" id="example-text-input">
                                            </div>
                                        </div>
                                        <div class="mb-3 row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Country Slide Images</label>
                                            <div class="col-md-8">
                                            <input type="file" multiple class="form-control" name="file[]" id="inputGroupFile02">
                                            </div>
                                        </div>
                                        <div class="mb-3 row">
                                            <div class="col-md-10">
                                                <button class="btn btn-primary" type="submit" name="islem">Ekle</button>
                                            </div>
                                            </form>
                            </div>
                        </div>
                    </div>
                </div>   

                </div>

                <?php include("./Layouts/footer.php") ?>
            