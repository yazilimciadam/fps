<?php
include("./Layouts/loginControl.php");
include("../classes/MainClass.php");
include("../classes/uploadImage/class.upload.php");
ini_set("upload_tmp_dir", "/tmp");
$veritabani = new MainClass();

if (isset($_POST["islem"]) && $_POST["islem"] == "ekle") {
    $image;
    $handle = new \Verot\Upload\Upload($_FILES["profile"],'tr_TR');
        if ($handle->uploaded) {
            $handle->file_new_name_body   = time();
            $handle->image_resize         = true;
            $handle->image_x              = 100;
            $handle->image_ratio_y        = true;
            $handle->file_src_pathname    = $_FILES["profile"]["tmp_name"];
            $handle->process("../uploads/");
            if ($handle->processed) {
               $image =  "/uploads/".$handle->file_dst_name;
               
            } else {
              echo 'error : ' . $handle->error;
            }
          }
          
    echo $veritabani->addTeam($_POST["name"], $image);

}
if (isset($_POST["islem"]) && $_POST["islem"] == "update") {
    $image;
    $handle = new \Verot\Upload\Upload($_FILES["profile"],'tr_TR');
        if ($handle->uploaded) {
            $handle->file_new_name_body   = time();
            $handle->image_resize         = true;
            $handle->image_x              = 100;
            $handle->image_ratio_y        = true;
            $handle->file_src_pathname    = $_FILES["profile"]["tmp_name"];
            $handle->process("../uploads/");
            if ($handle->processed) {
               $image =  "/uploads/".$handle->file_dst_name;
               
            } else {
              echo 'error : ' . $handle->error;
            }
          }
          
    $veritabani->updateTam($_POST["id"],$_POST["name"], $image);
}

?>

<?php include("./Layouts/header.php") ?>
<?php if (isset($_GET["update"])) {
    $client = $veritabani->getTeamOne($_GET["team_id"]);

?>
    <div class="page-content">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4> Team Update </h4>
                        <form  enctype="multipart/form-data"  action="/admin/addTeam.php" method="post">


                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Team User Name</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="name" placeholder="Bluemind" value="<?php echo $client["name"] ?>" id=" example-text-input">
                                </div>
                            </div>
                          
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Team User Profile Image </label>
                                <div class="col-md-8">
                                    <input type="file" name="profile" id="" class="form-control">

                                </div>
                            </div>


                            <div class="mb-3 row">
                                <div class="col-md-10">
                                    <input type="hidden" name="id" value="<?php echo $_GET["team_id"] ?>">
                                    <button class="btn btn-primary" type="submit" value="update" name="islem">Ekle</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="page-content">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4> Team User Add </h4>
                        <form enctype="multipart/form-data"  action="/admin/addTeam.php" method="post">

                        <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Team User Name</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="name" placeholder="Bluemind" id=" example-text-input">
                                </div>
                            </div>
                          
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Team User Profile Image </label>
                                <div class="col-md-8">
                                    <input type="file" name="profile" id="" class="form-control">

                                </div>
                            </div>


                            <div class="mb-3 row">
                                <div class="col-md-10">
                                   
                                    <button class="btn btn-primary" type="submit" value="ekle" name="islem">Ekle</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php include("./Layouts/footer.php") ?>