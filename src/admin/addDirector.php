<?php
include("./Layouts/loginControl.php");
include("../classes/MainClass.php");
include("../classes/uploadImage/class.upload.php");
ini_set("upload_tmp_dir", "/tmp");
$veritabani = new MainClass();

if (isset($_POST["islem"]) && $_POST["islem"] == "ekle") {
    $image;
    $image2
    $handle = new \Verot\Upload\Upload($_FILES["profile"],'tr_TR');
        if ($handle->uploaded) {
            $handle->file_new_name_body   = time();
            $handle->image_resize         = true;
            $handle->image_x              = 100;
            $handle->image_ratio_y        = true;
            $handle->file_src_pathname    = $_FILES["profile"]["tmp_name"];
            $handle->process("../uploads/");
            if ($handle->processed) {
               $image =  "/uploads/".$handle->file_dst_name;
               
            } else {
              echo 'error : ' . $handle->error;
            }
          }

    $handle2 = new \Verot\Upload\Upload($_FILES["about"],'tr_TR');
          if ($handle2->uploaded) {
              $handle2->file_new_name_body   = time();
              $handle2->image_resize         = true;
              $handle2->image_x              = 100;
              $handle2->image_ratio_y        = true;
              $handle2->file_src_pathname    = $_FILES["about"]["tmp_name"];
              $handle->process("../uploads/");
              if ($handle2->processed) {
                 $image2 =  "/uploads/".$handle2->file_dst_name;
                 
              } else {
                echo 'error : ' . $handle2->error;
              }
            }      
          
    echo $veritabani->addDirectors($_POST["name"], $_POST["mail"], $_POST["surname"], $_POST["position"], $image,$_POST["about"],$image2);

}
if (isset($_POST["islem"]) && $_POST["islem"] == "update") {
    $image;
    $image2;
    $handle = new \Verot\Upload\Upload($_FILES["profile"],'tr_TR');
        if ($handle->uploaded) {
            $handle->file_new_name_body   = time();
            $handle->image_resize         = true;
            $handle->image_x              = 100;
            $handle->image_ratio_y        = true;
            $handle->file_src_pathname    = $_FILES["profile"]["tmp_name"];
            $handle->process("../uploads/");
            if ($handle->processed) {
               $image =  "/uploads/".$handle->file_dst_name;
               
            } else {
              echo 'error : ' . $handle->error;
            }
          }


          $handle2 = new \Verot\Upload\Upload($_FILES["about"],'tr_TR');
          if ($handle2->uploaded) {
              $handle2->file_new_name_body   = time();
              $handle2->image_resize         = true;
              $handle2->image_x              = 100;
              $handle2->image_ratio_y        = true;
              $handle2->file_src_pathname    = $_FILES["about"]["tmp_name"];
              $handle->process("../uploads/");
              if ($handle2->processed) {
                 $image2 =  "/uploads/".$handle2->file_dst_name;
                 
              } else {
                echo 'error : ' . $handle2->error;
              }
            }      
          
    $veritabani->updateDirector($_POST["name"], $_POST["mail"], $_POST["surname"], $_POST["position"], $image, $_POST["id"], $_POST["about"],$image2);
}

?>

<?php include("./Layouts/header.php") ?>
<?php if (isset($_GET["update"])) {
    $client = $veritabani->getOneDirector($_GET["director_id"]);

?>
    <div class="page-content">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4> Director Update </h4>
                        <form  enctype="multipart/form-data"  action="/admin/addDirector.php" method="post">


                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Director Name</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="name" placeholder="Bluemind" value="<?php echo $client["name"] ?>" id=" example-text-input">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Director Surname</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" placeholder="tr" name="surname" value="<?php echo $client["surname"] ?>" id="example-text-input">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Director Mail </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" placeholder="Türkiye" name="mail" value="<?php echo $client["email"] ?>" id="example-text-input">

                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Director Position </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" placeholder="CEO" name="position" value="<?php echo $client["position"] ?>" id="example-text-input">

                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Director About </label>
                                <div class="col-md-8">
                                   <textarea name="about" id="" cols="30" rows="10" class="form-control"><?php echo $client["about"] ?></textarea>

                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Director Profile Image </label>
                                <div class="col-md-8">
                                    <input type="file" name="profile" id="" class="form-control">

                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Director About Image </label>
                                <div class="col-md-8">
                                    <input type="file" name="about" id="" class="form-control">

                                </div>
                            </div>


                            <div class="mb-3 row">
                                <div class="col-md-10">
                                    <input type="hidden" name="id" value="<?php echo $_GET["director_id"] ?>">
                                    <button class="btn btn-primary" type="submit" value="update" name="islem">Ekle</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="page-content">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4> Director Add </h4>
                        <form enctype="multipart/form-data"  action="/admin/addDirector.php" method="post">


                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Director Name</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="name" placeholder="Ali"  id=" example-text-input">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Director Surname</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" placeholder="Doner" name="surname"  id="example-text-input">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Director Mail </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" placeholder="ali@bluemind.agency" name="mail"  id="example-text-input">

                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Director Position </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" placeholder="CEO" name="position" id="example-text-input">

                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Director About </label>
                                <div class="col-md-8">
                                   <textarea name="about" id="" cols="30" rows="10" class="form-control"> </textarea>

                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Director Profile Image </label>
                                <div class="col-md-8">
                                    <input type="file" name="profile" id="" class="form-control">

                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Director About Image </label>
                                <div class="col-md-8">
                                    <input type="file" name="about" id="" class="form-control">

                                </div>
                            </div>


                            <div class="mb-3 row">
                                <div class="col-md-10">
                                   
                                    <button class="btn btn-primary" type="submit" value="ekle" name="islem">Ekle</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php include("./Layouts/footer.php") ?>