<?php
include("./Layouts/loginControl.php");
include("../classes/MainClass.php");

$veritabani = new MainClass();

if (isset($_POST["islem"]) && $_POST["islem"] == "ekle") {
    $veritabani->addClient($_POST["client_name"], $_POST["country_code"], $_POST["country"], $_POST["position"], $_POST["person"], $_POST["title"], $_POST["comment"]);
    
}
if (isset($_POST["islem"]) && $_POST["islem"] == "update") {
    $veritabani->updateClient($_POST["client_name"], $_POST["country_code"], $_POST["country"], $_POST["position"], $_POST["person"], $_POST["title"], $_POST["comment"], $_POST["client_id"]);
    
}   

?>

<?php include("./Layouts/header.php") ?>
<?php if(isset($_GET["update"])){
$client = $veritabani->getOneClient($_GET["client_id"]);

 ?>
 <div class="page-content">

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4> Client Update </h4>
                <form action="/admin/addClient.php" method="post">


                    <div class="mb-3 row">
                        <label for="example-text-input" class="col-md-2 col-form-label">Client Title</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="client_name" placeholder="Bluemind" value="<?php echo $client["client_name"] ?>" id=" example-text-input">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="example-text-input" class="col-md-2 col-form-label">Country Code</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" placeholder="tr" name="country_code" value="<?php echo $client["client_country_code"] ?>" id="example-text-input">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="example-text-input" class="col-md-2 col-form-label">Country </label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" placeholder="Türkiye" name="country" value="<?php echo $client["client_country"] ?>" id="example-text-input">

                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="example-text-input" class="col-md-2 col-form-label">Client Personel </label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" placeholder="JOHN Doe" name="person" value="<?php echo $client["client_person"] ?>" id="example-text-input">

                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="example-text-input" class="col-md-2 col-form-label">Client Personel Position </label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" placeholder="CEO" name="position" value="<?php echo $client["client_position"] ?>" id="example-text-input">

                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="example-text-input" class="col-md-2 col-form-label">Client Comment Title </label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" placeholder="Harikaaaa !" name="title" value="<?php echo $client["client_comment_title"] ?>" id="example-text-input">

                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="example-text-input" class="col-md-2 col-form-label">Client Comment </label>
                        <div class="col-md-8">
                            <textarea name="comment" id="" cols="30" rows="10" class="form-control"> <?php echo $client["client_comment"] ?></textarea>

                        </div>
                    </div>

                    <div class="mb-3 row">
                        <div class="col-md-10">
                            <input type="hidden" name="client_id" value="<?php echo $_GET["client_id"] ?>">
                            <button class="btn btn-primary" type="submit" value="update" name="islem">Güncelle</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<?php }else{ ?>
<div class="page-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4> Client Add </h4>
                    <form action="/admin/addClient.php" method="post">


                        <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Client Title</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" name="client_name" placeholder="Bluemind"" id=" example-text-input">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Country Code</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" placeholder="tr" name="country_code" id="example-text-input">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Country </label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" placeholder="Türkiye" name="country" id="example-text-input">

                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Client Personel </label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" placeholder="JOHN Doe" name="person" id="example-text-input">

                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Client Personel Position </label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" placeholder="CEO" name="position" id="example-text-input">

                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Client Comment Title </label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" placeholder="Harikaaaa !" name="title" id="example-text-input">

                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Client Comment </label>
                            <div class="col-md-8">
                                <textarea name="comment" id="" cols="30" rows="10" class="form-control"></textarea>

                            </div>
                        </div>

                        <div class="mb-3 row">
                            <div class="col-md-10">
                                <button class="btn btn-primary" type="submit" value="ekle" name="islem">Ekle</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }?>
<?php include("./Layouts/footer.php") ?>