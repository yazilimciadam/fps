<?php
include("./Layouts/loginControl.php");
include("../classes/MainClass.php");
include("../classes/uploadImage/class.upload.php");
ini_set("upload_tmp_dir", "/tmp");
$veritabani = new MainClass();
$site_info = $veritabani->getSiteinfo();


if (isset($_POST["islem"]) && $_POST["islem"] == "update") {
 
    echo $veritabani->updateSiteInfo($_POST["facebook"], $_POST["twitter"], $_POST["instagram"], $_POST["desc"], $_POST["about"], $_POST["home_title"], $_POST["home_title_sub"], $_POST["menu_sub"], $_POST["title"]);
}

?>

<?php include("./Layouts/header.php") ?>
<?php if (isset($_GET["update"])) {
   $client = $veritabani->getSiteinfo();


?>
    <div class="page-content">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4> Service Update </h4>
                        <form   action="/admin/site_info.php" method="post">

                        <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Site Title</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="title" placeholder="FPS" value="<?php echo $client["title"] ?>" id=" example-text-input">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Site Desc</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="desc" placeholder="Description" value="<?php echo $client["desc_site"] ?>" id=" example-text-input">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">HomePage Title</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="home_title" placeholder="Its Home" value="<?php echo $client["index_title_big"] ?>" id=" example-text-input">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">HomePage Sub Title</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="home_title_sub" placeholder="ITSH Home Sub" value="<?php echo $client["index_title_sub"] ?>" id=" example-text-input">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Menu Sub Title</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="menu_sub" placeholder="About" value="<?php echo $client["menu_subtitle"] ?>" id=" example-text-input">
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">About</label>
                                <div class="col-md-10">
                                   <textarea class="form-control" name="about" id="" cols="30" rows="10"><?php echo $client["about"] ?></textarea>
                                </div>
                            </div>
                            
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Twitter Link</label>
                                <div class="col-md-2">
                                    <input class="form-control" type="text" name="twitter" placeholder="Bluemind" value="<?php echo $client["twitter"] ?>" id=" example-text-input">
                                </div>
                                <label for="example-text-input" class="col-md-2 col-form-label">Facebook Link</label>
                                <div class="col-md-2">
                                    <input class="form-control" type="text" name="facebook" placeholder="Bluemind" value="<?php echo $client["facebook"] ?>" id=" example-text-input">
                                </div>
                                <label for="example-text-input" class="col-md-2 col-form-label">Instagram Link</label>
                                <div class="col-md-2">
                                    <input class="form-control" type="text" name="instagram" placeholder="Bluemind" value="<?php echo $client["instagram"] ?>" id=" example-text-input">
                                </div>
                            </div>


                            <div class="mb-3 row">
                                <div class="col-md-10">
                                    
                                    <button class="btn btn-primary" type="submit" value="update" name="islem">Ekle</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

   
<?php } ?>
<?php include("./Layouts/footer.php") ?>