<li>
                                <a href="/admin/dashboard" class="waves-effect">
                                    <i class="bx bx-home-circle"></i>
                                    <span key="t-dashboards">Dashboard</span>
                                </a>
                              
                                   
                            </li>
                            <li >
                                <a href="javascript: void(0);" class="waves-effect mm-active">
                                    <i class="dripicons-flag"></i>
                                    <span key="t-dashboards">Country</span>
                                </a>
                                <ul class="sub-menu mm-collapse mm-show" aria-expanded="false">
                                    <li class="mm-active"><a href="/admin/addCountry" key="t-default" >Add Country</a></li>
                                    <li><a href="/admin/getCountry" key="t-saas">Countrys</a></li>
                                   
                                </ul>
                            </li>
                            <li >
                                <a href="javascript: void(0);" class="waves-effect" aria-expanded="true">
                                    <i class="bx bx-store"></i>
                                    <span key="t-dashboards">Clients</span>
                                </a>
                                <ul class="sub-menu mm-collapse" >
                                    <li class="mm-active"><a href="/admin/addClient" key="t-default" >Add Client</a></li>
                                    <li><a href="/admin/getClients" key="t-saas">Clients</a></li>
                                   
                                </ul>
                            </li>
                            <li >
                                <a href="javascript: void(0);" class="waves-effect mm-active">
                                    <i class="bx bx-user"></i>
                                    <span key="t-dashboards">Directors</span>
                                </a>
                                <ul class="sub-menu mm-collapse mm-show" aria-expanded="false">
                                    <li class="mm-active"><a href="/admin/addDirector" key="t-default" >Add Director</a></li>
                                    <li><a href="/admin/getDirectors" key="t-saas">Directors</a></li>
                                   
                                </ul>
                            </li>
                            <li >
                                <a href="javascript: void(0);" class="waves-effect mm-active">
                                    <i class="dripicons-direction"></i>
                                    <span key="t-dashboards">Locations</span>
                                </a>
                                <ul class="sub-menu mm-collapse mm-show" aria-expanded="false">
                                    <li class="mm-active"><a href="/admin/addLocation" key="t-default" >Add Locations</a></li>
                                    <li><a href="/admin/getLocation" key="t-saas">Locations</a></li>
                                   
                                </ul>
                            </li>
                            <li >
                                <a href="javascript: void(0);" class="waves-effect mm-active">
                                    <i class="dripicons-direction"></i>
                                    <span key="t-dashboards">Productions</span>
                                </a>
                                <ul class="sub-menu mm-collapse mm-show" aria-expanded="false">
                                    <li class="mm-active"><a href="/admin/addProduction" key="t-default" >Add Production</a></li>
                                    <li><a href="/admin/getProd" key="t-saas">Productions</a></li>
                                   
                                </ul>
                            </li>
                            <li >
                                <a href="javascript: void(0);" class="waves-effect mm-active">
                                    <i class="dripicons-direction"></i>
                                    <span key="t-dashboards">Services</span>
                                </a>
                                <ul class="sub-menu mm-collapse mm-show" aria-expanded="false">
                                    <li class="mm-active"><a href="/admin/addService" key="t-default" >Add Service</a></li>
                                    <li><a href="/admin/getServices" key="t-saas">Services</a></li>
                                   
                                </ul>
                            </li>

                            <li >
                                <a href="javascript: void(0);" class="waves-effect mm-active">
                                    <i class="dripicons-direction"></i>
                                    <span key="t-dashboards">WHy</span>
                                </a>
                                <ul class="sub-menu mm-collapse mm-show" aria-expanded="false">
                                    <li class="mm-active"><a href="/admin/addWhy" key="t-default" >Add WHY</a></li>
                                    <li><a href="/admin/getWhy" key="t-saas">All Why</a></li>
                                   
                                </ul>
                            </li>