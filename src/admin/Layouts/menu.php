<li>
                                <a href="/admin/" class="waves-effect">
                                    <i class="bx bx-home-circle"></i>
                                    <span key="t-dashboards">Dashboard</span>
                                </a>
                              
                                   
                            </li>
                            <li >
                                <a href="javascript: void(0);" class="waves-effect mm-active">
                                    <i class="dripicons-flag"></i>
                                    <span key="t-dashboards">Country</span>
                                </a>
                                <ul class="sub-menu mm-collapse mm-show" aria-expanded="false">
                                    <li class="mm-active"><a href="/admin/addCountry" key="t-default" >Add Country</a></li>
                                    <li><a href="/admin/getCountry" key="t-saas">Countrys</a></li>
                                   
                                </ul>
                            </li>
                            <li >
                                <a href="javascript: void(0);" class="waves-effect" aria-expanded="true">
                                    <i class="bx bx-store"></i>
                                    <span key="t-dashboards">Clients</span>
                                </a>
                                <ul class="sub-menu mm-collapse" >
                                    <li class="mm-active"><a href="/admin/addClient" key="t-default" >Add Client</a></li>
                                    <li><a href="/admin/getClient" key="t-saas">Clients</a></li>
                                   
                                </ul>
                            </li>
                            <li >
                                <a href="javascript: void(0);" class="waves-effect mm-active">
                                    <i class="bx bx-user"></i>
                                    <span key="t-dashboards">Directors</span>
                                </a>
                                <ul class="sub-menu mm-collapse mm-show" aria-expanded="false">
                                    <li class="mm-active"><a href="/admin/addDirector" key="t-default" >Add Director</a></li>
                                    <li><a href="/admin/getDirectors" key="t-saas">Directors</a></li>
                                   
                                </ul>
                            </li>
                          
                            <li >
                                <a href="javascript: void(0);" class="waves-effect mm-active">
                                    <i class="dripicons-direction"></i>
                                    <span key="t-dashboards">Productions</span>
                                </a>
                                <ul class="sub-menu mm-collapse mm-show" aria-expanded="false">
                                    <li class="mm-active"><a href="/admin/addProduction" key="t-default" >Add Production</a></li>
                                    <li><a href="/admin/getProd" key="t-saas">Productions</a></li>
                                    <li class="mm-active"><a href="/admin/addLocation" key="t-default" >Add Locations</a></li>
                                    <li><a href="/admin/getLocation" key="t-saas">Locations</a></li>
                                   
                                </ul>
                            </li>
                            <li >
                                <a href="javascript: void(0);" class="waves-effect mm-active">
                                    <i class="dripicons-direction"></i>
                                    <span key="t-dashboards">Services</span>
                                </a>
                                <ul class="sub-menu mm-collapse mm-show" aria-expanded="false">
                                    <li class="mm-active"><a href="/admin/addService" key="t-default" >Add Service</a></li>
                                    <li><a href="/admin/getServices" key="t-saas">Services</a></li>
                                   
                                </ul>
                            </li>

                            <li >
                                <a href="javascript: void(0);" class="waves-effect mm-active">
                                    <i class="dripicons-direction"></i>
                                    <span key="t-dashboards">WHy</span>
                                </a>
                                <ul class="sub-menu mm-collapse mm-show" aria-expanded="false">
                                    <li class="mm-active"><a href="/admin/addWhy" key="t-default" >Add WHY</a></li>
                                    <li><a href="/admin/getWhy" key="t-saas">All Why</a></li>
                                   
                                </ul>
                            </li>

                            <li >
                                <a href="javascript: void(0);" class="waves-effect mm-active">
                                    <i class="dripicons-direction"></i>
                                    <span key="t-dashboards">Contacts</span>
                                </a>
                                <ul class="sub-menu mm-collapse mm-show" aria-expanded="false">
                                    <li class="mm-active"><a href="/admin/addContact" key="t-default" >Add Contact</a></li>
                                    <li><a href="/admin/getContact" key="t-saas">All Contac</a></li>
                                   
                                </ul>
                            </li>

                            <li >
                                <a href="javascript: void(0);" class="waves-effect mm-active">
                                    <i class="dripicons-direction"></i>
                                    <span key="t-dashboards">Teams</span>
                                </a>
                                <ul class="sub-menu mm-collapse mm-show" aria-expanded="false">
                                    <li class="mm-active"><a href="/admin/addTeam" key="t-default" >Add Teams</a></li>
                                    <li><a href="/admin/getTeam" key="t-saas">All Team</a></li>
                                   
                                </ul>
                            </li>


                            <li >
                                <a href="javascript: void(0);" class="waves-effect mm-active">
                                    <i class="dripicons-direction"></i>
                                    <span key="t-dashboards">Site Settings</span>
                                </a>
                                <ul class="sub-menu mm-collapse mm-show" aria-expanded="false">
                                    <li class="mm-active"><a href="/admin/site_info?update=1" key="t-default" >Update Site Settings</a></li>
                                  
                                   
                                </ul>
                            </li>