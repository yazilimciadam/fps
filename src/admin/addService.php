<?php
include("./Layouts/loginControl.php");
include("../classes/MainClass.php");
include("../classes/uploadImage/class.upload.php");
ini_set("upload_tmp_dir", "/tmp");
$veritabani = new MainClass();
$country = $veritabani->getCountrys();
//var_dump($_POST);
if (isset($_POST["islem"]) && $_POST["islem"] == "ekle") {
    $image;
    $handle = new \Verot\Upload\Upload($_FILES["profile"],'tr_TR');
        if ($handle->uploaded) {
            $handle->file_new_name_body   = time();
            $handle->image_resize         = true;
            $handle->image_x              = 100;
            $handle->image_ratio_y        = true;
            $handle->file_src_pathname    = $_FILES["profile"]["tmp_name"];
            $handle->process("../uploads/");
            if ($handle->processed) {
               $image =  "/uploads/".$handle->file_dst_name;
               
            } else {
              echo 'error : ' . $handle->error;
            }
          }
          
    echo $veritabani->addServices($_POST["name"], $_POST["detail"], $_POST["country_id"]);

}
if (isset($_POST["islem"]) && $_POST["islem"] == "update") {
    $image;
    $handle = new \Verot\Upload\Upload($_FILES["profile"],'tr_TR');
        if ($handle->uploaded) {
            $handle->file_new_name_body   = time();
            $handle->image_resize         = true;
            $handle->image_x              = 100;
            $handle->image_ratio_y        = true;
            $handle->file_src_pathname    = $_FILES["profile"]["tmp_name"];
            $handle->process("../uploads/");
            if ($handle->processed) {
               $image =  "/uploads/".$handle->file_dst_name;
               
            } else {
              echo 'error : ' . $handle->error;
            }
          }
          
    $veritabani->updateServices($_POST["name"], $_POST["detail"], $_POST["country_id"], $_POST["id"],);
}

?>

<?php include("./Layouts/header.php") ?>
<?php if (isset($_GET["update"])) {
    $client = $veritabani->getServicesOne($_GET["service_id"]);

?>
    <div class="page-content">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4> Service Update </h4>
                        <form   action="/admin/addService.php" method="post">


                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Service Title</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="name" placeholder="Bluemind" value="<?php echo $client["service_title"] ?>" id=" example-text-input">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Service Country</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="country_id" id="">
                                        <?php for ($i=0; $i < count($country); $i++) { 
                                            
                                            echo '<option value="'.$country[$i]["country_id"].'">'.$country[$i]["country"].'</option>';

                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Service Detail </label>
                                <div class="col-md-8">
                                    
                                    <textarea class="form-control" name="detail" id="" cols="30" rows="10"><?php echo $client["service_detail"] ?></textarea>
                                </div>
                            </div>


                            <div class="mb-3 row">
                                <div class="col-md-10">
                                    <input type="hidden" name="id" value="<?php echo $_GET["service_id"] ?>">
                                    <button class="btn btn-primary" type="submit" value="update" name="islem">Ekle</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="page-content">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4> Service Update </h4>
                        <form  action="/admin/addService.php" method="post">


                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Service Title</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="name" placeholder="Ali" " id=" example-text-input">
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Service Country</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="country_id" id="">
                                        <?php for ($i=0; $i < count($country); $i++) { 
                                            
                                            echo '<option value="'.$country[$i]["country_id"].'">'.$country[$i]["country"].'</option>';

                                        } ?>
                                    </select>
                                </div>
                            </div>
                            
                          
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Service Detail </label>
                                <div class="col-md-8">
                                <textarea class="form-control" name="detail" id="" cols="30" rows="10"></textarea>

                                </div>
                            </div>


                            <div class="mb-3 row">
                                <div class="col-md-10">
                                   
                                    <button class="btn btn-primary" type="submit" value="ekle" name="islem">Ekle</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php include("./Layouts/footer.php") ?>