<?php
include("./Layouts/loginControl.php");
include("../classes/MainClass.php");
include("../classes/uploadImage/class.upload.php");
//include("../classes/uploadImage/FFmpeg.php");


$veritabani = new MainClass();
$country = $veritabani->getCountrys();
//var_dump($_POST);
//var_dump($_FILES);
if (isset($_POST["islem"]) && $_POST["islem"] == "ekle") {
    $image;
    $video;
    $videoPath;
    $handle = new \Verot\Upload\Upload($_FILES["profile"],'tr_TR');
    $handleVideo = new \Verot\Upload\Upload($_FILES["profileVideo"],'tr_TR');
        if ($handle->uploaded) {
            $handle->file_new_name_body   = time();
            $handle->image_resize         = true;
            $handle->image_x              = 100;
            $handle->image_ratio_y        = true;
            $handle->file_src_pathname    = $_FILES["profile"]["tmp_name"];
            $handle->process("../uploads/");
            if ($handle->processed) {
               $image =  "/uploads/".$handle->file_dst_name;
               
            } else {
              echo 'error : ' . $handle->error;
            }
          }
       
        if ($handleVideo->uploaded) {
            $handleVideo->file_new_name_body   = time()*round(1,30);
            $handleVideo->file_src_pathname    = $_FILES["profileVideo"]["tmp_name"];
           
            $handleVideo->process("../uploads/");
            if ($handleVideo->processed) {
               
                $video = "/uploads/".$handleVideo->file_dst_name;
                $videoPath = $handleVideo->file_dst_pathname;
                
               
            } else {
              echo 'error : ' . $handleVideo->error;
            }
          }  
         
    $combineperson = array_combine($_POST["person_position"], $_POST["person_name"]);
    $data = json_encode($combineperson,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);

    
$files = array();
foreach ($_FILES['file'] as $k => $l) {
  foreach ($l as $i => $v) {
  if (!array_key_exists($i, $files))
    $files[$i] = array();
    $files[$i][$k] = $v;
  }
}

foreach ($files as $file ) {
    
    $handle3 = new \Verot\Upload\Upload($file,'tr_TR');
    if ($handle3->uploaded) {
        $handle3->file_new_name_body   = time();
        $handle3->image_resize         = true;
        $handle3->image_x              = 100;
        $handle3->image_ratio_y        = true;
        $handle3->file_src_pathname    = $file["tmp_name"];
        $handle3->process("../uploads/");
        if ($handle3->processed) {
        $arr[] = array(
            "image" => "/uploads/".$handle3->file_dst_name,
        );
        var_dump($arr);
         
        } else {
          echo 'error : ' . $handle3->error;
        }
      }
      unset($handle3);
}
    $images_behind = json_encode($arr);
    $arraycomment = array(
        "comment" => $_POST["comment"],
        "firm" => $_POST["firm"],
        "person" => $_POST["person"],
        "position" => $_POST["position"],
        "title"=>$_POST["title"],
    );
    $comment = json_encode($arraycomment,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);

    echo $veritabani->addProduction($_POST["name"], $image, $_POST["country_id"],$_POST["local_inter"],$_POST["isVideo"],$video, $_POST["isFeature"],$data, $_POST["film_tv"],$images_behind,$comment);

}
if (isset($_POST["islem"]) && $_POST["islem"] == "update") {
    $image;
    $video;
    $videoPath;
        if ($_FILES["profile"]["size"] > 0) {
            $handle = new \Verot\Upload\Upload($_FILES["profile"],'tr_TR');
    
        if ($handle->uploaded) {
            $handle->file_new_name_body   = time();
            $handle->image_resize         = true;
            $handle->image_x              = 100;
            $handle->image_ratio_y        = true;
            $handle->file_src_pathname    = $_FILES["profile"]["tmp_name"];
            $handle->process("../uploads/");
            if ($handle->processed) {
               $image =  "/uploads/".$handle->file_dst_name;
               
            } else {
              echo 'error : ' . $handle->error;
            }
          }
            
        } else {
            $image = $_POST["old_image"];
        }

         if ($_FILES["profileVideo"]["size"] > 0) {
            $handleVideo = new \Verot\Upload\Upload($_FILES["profileVideo"],'tr_TR');
            if ($handleVideo->uploaded) {
              $handleVideo->file_new_name_body   = time()*round(1,30);
              $handleVideo->file_src_pathname    = $_FILES["profileVideo"]["tmp_name"];
             
              $handleVideo->process("../uploads/");
              if ($handleVideo->processed) {
                 
                  $video = "/uploads/".$handleVideo->file_dst_name;
                  $videoPath = $handleVideo->file_dst_pathname;
                  
                 
              } else {
                echo 'error : ' . $handleVideo->error;
              }
            }  
         }else {
            $video = $_POST["old_video"];
        }
          $combineperson = array_combine($_POST["person_position"], $_POST["person_name"]);
          $data = json_encode($combineperson,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
    
           
$files = array();
foreach ($_FILES['file'] as $k => $l) {
  foreach ($l as $i => $v) {
  if (!array_key_exists($i, $files))
    $files[$i] = array();
    $files[$i][$k] = $v;
  }
}

foreach ($files as $file ) {
    
    $handle3 = new \Verot\Upload\Upload($file,'tr_TR');
    if ($handle3->uploaded) {
        $handle3->file_new_name_body   = time();
        $handle3->image_resize         = true;
        $handle3->image_x              = 100;
        $handle3->image_ratio_y        = true;
        $handle3->file_src_pathname    = $file["tmp_name"];
        $handle3->process("../uploads/");
        if ($handle3->processed) {
        $arr[] = array(
            "image" => "/uploads/".$handle3->file_dst_name,
        );
        var_dump($arr);
         
        } else {
          echo 'error : ' . $handle3->error;
        }
      }
      unset($handle3);
}
    $images_behind = json_encode($arr);
    $arraycomment = array(
        "comment" => $_POST["comment"],
        "firm" => $_POST["firm"],
        "person" => $_POST["person"],
        "position" => $_POST["position"],
        "title"=>$_POST["title"],
    );
    $comment = json_encode($arraycomment,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);


    $veritabani->updateProduction($_POST["name"], $image, $_POST["country_id"], $_POST["id"],$_POST["local_inter"],$_POST["isVideo"],$video, $_POST["isFeature"],$data,$_POST["film_tv"], $images_behind, $comment);


    

}

?>


<?php include("./Layouts/header.php") ?>
<?php if (isset($_GET["update"])) {
    $client = $veritabani->getProductionOne($_GET["production_id"]);
    $comment = json_decode($client["comment"]);

?>
    <div class="page-content">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4> Production Update </h4>
                        <form  enctype="multipart/form-data"  action="/admin/addProduction.php" method="post">


                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Production Title</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="name" placeholder="Bluemind" value="<?php echo $client["production_title"] ?>" id=" example-text-input">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Country</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="country_id" id="">
                                        <?php for ($i=0; $i < count($country); $i++) { 
                                            
                                            echo '<option value="'.$country[$i]["country_id"].'">'.$country[$i]["country"].'</option>';

                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Video Or Photo</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="isVideo" id="">
                                        <option value="1">Video</option>
                                        <option value="0">Photo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Featured</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="isFeature" id="">
                                        <option value="1">is Featured</option>
                                        <option value="0">Not Featured</option>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Video</label>
                                <div class="col-md-10">
                                <input type="file" name="profileVideo" id="" class="form-control">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Local/International</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="local_inter" id="">
                                        <option value="1">INTERNATIONAL</option>
                                        <option value="0">LOCAL</option>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Film/TV</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="film_tv" id="">
                                        <option value="1">Film</option>
                                        <option value="0">TV</option>
                                    </select>
                                </div>
                            </div>
                            <div  id="personlabel">
                            
                                    </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Production Photo </label>
                                <div class="col-md-8">
                                    <input type="file" name="profile" id="" class="form-control">

                                </div>
                            </div>
                            <h4>Comment ve Behind Bölümü</h4>

                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Behind İmages (600X600) </label>
                                <div class="col-md-8">
                                    <input type="file" name="file[]" multiple id=""  class="form-control">

                                </div>
                            </div>

                            <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Firm </label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" placeholder="LG" name="firm" value="<?php echo $comment->firm ?>" id="example-text-input">

                            </div>
                        </div>

                            <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Personel </label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" placeholder="JOHN Doe" name="person" value="<?php echo $comment->person ?>" id="example-text-input">

                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Personel Position </label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" placeholder="CEO" name="position" value="<?php echo $comment->position ?>" id="example-text-input">

                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Comment Title </label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" placeholder="Harikaaaa !" name="title" value="<?php echo $comment->title ?>" id="example-text-input">

                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Comment </label>
                            <div class="col-md-8">
                                <textarea name="comment" id="" cols="30" rows="10" class="form-control"> <?php echo $comment->comment ?></textarea>

                            </div>
                        </div>


                            <div class="mb-3 row">
                                <div class="col-md-10">
                                    <input type="hidden" name="id" value="<?php echo $_GET["production_id"] ?>">
                                    <input type="hidden" name="old_image" value="<?php echo $client["production_photo"] ?>">
                                    <input type="hidden" name="old_video" value="<?php echo $client["videoLink"] ?>">
                                    <button class="btn btn-primary" type="submit" value="update" name="islem">Ekle</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        var jsonData = <?php echo $client["persons"] ?>;
        Object.keys((jsonData)).map(function(data) {  

            $('#personlabel').append(`
            <div class="mb-3 row" id="person">
            <label id=""  for="example-text-input" class="col-md-2 col-form-label">Persons <i onclick="clone()" class="btn btn-primary dripicons-plus"></i></label>
                                
            <div class="col-md-5">
               
            <input type="text"  class="form-control" name="person_position[]" value="`+data +`" />
            </div>
            
            <div class="col-md-5">
                <input type="text" placeholder="Name" name="person_name[]" value="`+jsonData[data]+`" class="form-control">
            </div>
            
            
            `)
        })

        function clone() {
            
            var real = $("#person");
            var cloned = real.clone(true);
            cloned.insertAfter(real);   
            }
    

    </script>


<?php } else { ?>
    <div class="page-content">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4> Production Add </h4>
                        <form enctype="multipart/form-data"  action="/admin/addProduction.php" method="post">


                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Production Title</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="name" placeholder="Ali"  id=" example-text-input">
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Production Country</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="country_id" id="">
                                        <?php for ($i=0; $i < count($country); $i++) { 
                                            
                                            echo '<option value="'.$country[$i]["country_id"].'">'.$country[$i]["country"].'</option>';

                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Featured</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="isFeature" id="">
                                        <option value="1">is Featured</option>
                                        <option value="0">Not Featured</option>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Video Or Photo</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="isVideo" id="">
                                        <option value="1">Video</option>
                                        <option value="0">Photo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Video</label>
                                <div class="col-md-10">
                                <input type="file" name="profileVideo" id="" class="form-control">
                                </div>
                            </div>
                            
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Local/International</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="local_inter" id="">
                                        <option value="1">INTERNATIONAL</option>
                                        <option value="0">LOCAL</option>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Film/TV</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="film_tv" id="">
                                        <option value="1">Film</option>
                                        <option value="0">TV</option>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row"  id="person">
                                <label for="example-text-input" class="col-md-2 col-form-label">Persons <i onclick="clone()" class="btn btn-primary dripicons-plus"></i></label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="Position" name="person_position[]" class="form-control">
                                    
                                </div>
                                <div class="col-md-5">
                                    <input type="text" placeholder="Name" name="person_name[]" class="form-control">
                                    
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Production Images </label>
                                <div class="col-md-8">
                                    <input type="file" name="profile" id="" class="form-control">

                                </div>
                            </div>
                            <hr>
                            <h4>Comment ve Behind Bölümü</h4>

                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Behind İmages (600X600) </label>
                                <div class="col-md-8">
                                    <input type="file" name="file[]" multiple id=""  class="form-control">

                                </div>
                            </div>

                            <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Firm </label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" placeholder="LG" name="firm" id="example-text-input">

                            </div>
                        </div>

                            <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Personel </label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" placeholder="JOHN Doe" name="person" id="example-text-input">

                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Personel Position </label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" placeholder="CEO" name="position" id="example-text-input">

                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Comment Title </label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" placeholder="Harikaaaa !" name="title" id="example-text-input">

                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Comment </label>
                            <div class="col-md-8">
                                <textarea name="comment" id="" cols="30" rows="10" class="form-control"></textarea>

                            </div>
                        </div>





                            <div class="mb-3 row">
                                <div class="col-md-10">
                                   
                                    <button class="btn btn-primary" type="submit" value="ekle" name="islem">Ekle</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<?php } ?>
<script>

        function clone() {
            
        var real = $("#person");
        var cloned = real.clone(true);
        cloned.insertAfter(real);   
        }



    </script>

<?php include("./Layouts/footer.php") ?>