<?php 
include("./Layouts/loginControl.php");
include("../classes/MainClass.php");
include("../classes/uploadImage/class.upload.php");

$veritabani = new MainClass();

$all = $veritabani->getCountrys();
$allLoc = $veritabani->getContactAll();

if ($_POST["islem"] == "sil") {
    $veritabani->deleteContact($_POST["id"]);
    # code...
}



?>

<?php  include("./Layouts/header.php") ?>

                <div class="page-content">
                   
                   <div class="col-md-12">
                   <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">Clients</h4>
                                        
                                        <div class="table-responsive">
                                            <table class="table mb-0">
        
                                                <thead class="table-light">
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Tıtle</th>
                                                        <th>desc</th>
                                                        <th>Address</th>
                                                        <th>Phone</th>
                                                        <th>map</th>
                                                        <th>Delete</th>
                                                        <th>Edit</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php 
                                                $sayi = 0;
                                                for ($i=0; $i < count($allLoc); $i++) { 
                                                    $ulke = $allLoc[$i];
                                                 ?>   
                                                <tr>
                                                        <th scope="row"><?php echo $sayi+=1 ?></th>
                                                        
                                                        <td> <?php echo  $ulke["title"] ?></td>
                                                        <td> <?php echo  $ulke["description"] ?></td>
                                                        <td> <?php echo  $ulke["address"] ?></td>
                                                        <td> <?php echo  $ulke["phone"] ?></td>
                                                        <td> <?php echo  $ulke["map"] ?></td>
                                                        <td> 
                                                            <form action="/admin/getContact.php" method="post">
                                                                <input type="hidden" name="id" value="<?php echo $ulke["contact_id"] ?>">
                                                                <input type="hidden" name="islem" value="sil">
                                                                <button type="submit" class="btn btn-danger">Sil</button>
                                                            </form>
                                                            
                                                        </td>
                                                        <td> <form action="/admin/addContact.php" method="get">
                                                                <input type="hidden" name="contact_id" value="<?php echo $ulke["contact_id"] ?>">
                                                                <input type="hidden" name="islem" value="update">
                                                                <input type="hidden" name="update" value="1">
                                                                <button type="submit" class="btn btn-warning">Update</button>
                                                            </form></td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
        
                                    </div>
                                </div>
                   </div>
                </div>

                <?php include("./Layouts/footer.php") ?>
            