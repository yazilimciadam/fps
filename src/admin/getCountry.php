<?php 
include("./Layouts/loginControl.php");
include("../classes/MainClass.php");
include("../classes/uploadImage/class.upload.php");

$veritabani = new MainClass();

$all = $veritabani->getCountrys();

if($_POST["islem"] == "sil"){
    $veritabani->deleteCountry($_POST["id"]);
}


?>

<?php  include("./Layouts/header.php") ?>

                <div class="page-content">
                   
                   <div class="col-md-12">
                   <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">Ülkeler</h4>
                                        
                                        <div class="table-responsive">
                                            <table class="table mb-0">
        
                                                <thead class="table-light">
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Ülke</th>
                                                        <th>Edit/Delete</th>
                                                        <th>Show</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php 
                                                $sayi = 0;
                                                for ($i=0; $i < count($all); $i++) { 
                                                    $ulke = $all[$i];
                                                 ?>   
                                                <tr>
                                                        <th scope="row"><?php echo $sayi+=1 ?></th>
                                                        
                                                        <td> <?php echo  $ulke["country"] ?></td>
                                                        <td> 
                                                            <form action="/admin/getCountry.php" method="post">
                                                                <input type="hidden" name="id" value="<?php echo $ulke["country_id"] ?>">
                                                                <input type="hidden" name="islem" value="sil">
                                                                <button type="submit" class="btn btn-danger">Sil</button>
                                                            </form>
                                                        </td>
                                                        <td> <a class="btn btn-primary" href="/Country/<?php echo $ulke["country"] ?>">Show</a> </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
        
                                    </div>
                                </div>
                   </div>
                </div>

                <?php include("./Layouts/footer.php") ?>
            