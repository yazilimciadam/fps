<?php 
include("./Layouts/loginControl.php");
include("../classes/MainClass.php");
include("../classes/uploadImage/class.upload.php");

$veritabani = new MainClass();

$all = $veritabani->getCountrys();
$allLoc = $veritabani->getAllLocation();

if ($_POST["islem"] == "sil") {
    $veritabani->deleteLocation($_POST["id"]);
    # code...
}



?>

<?php  include("./Layouts/header.php") ?>

                <div class="page-content">
                   
                   <div class="col-md-12">
                   <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">Clients</h4>
                                        
                                        <div class="table-responsive">
                                            <table class="table mb-0">
        
                                                <thead class="table-light">
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Location Name</th>
                                                        <th>Country</th>
                                                       
                                                        <th>Delete</th>
                                                        <th>Edit</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php 
                                                $sayi = 0;
                                                for ($i=0; $i < count($allLoc); $i++) { 
                                                    $ulke = $allLoc[$i];
                                                 ?>   
                                                <tr>
                                                        <th scope="row"><?php echo $sayi+=1 ?></th>
                                                        
                                                        <td> <?php echo  $ulke["location"] ?></td>
                                                        <td> <?php 
                                                       $aab = array_search($ulke["country_id"], array_column($all, 'country_id'));
                                                         echo $all[$aab]["country"];

                                                        ?></td>
                                                       
                                                        <td> 
                                                            <form action="/admin/getLocation.php" method="post">
                                                                <input type="hidden" name="id" value="<?php echo $ulke["location_id"] ?>">
                                                                <input type="hidden" name="islem" value="sil">
                                                                <button type="submit" class="btn btn-danger">Sil</button>
                                                            </form>
                                                            
                                                        </td>
                                                        <td> <form action="/admin/addLocation.php" method="get">
                                                                <input type="hidden" name="location_id" value="<?php echo $ulke["location_id"] ?>">
                                                                <input type="hidden" name="islem" value="update">
                                                                <input type="hidden" name="update" value="1">
                                                                <button type="submit" class="btn btn-warning">Update</button>
                                                            </form></td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
        
                                    </div>
                                </div>
                   </div>
                </div>

                <?php include("./Layouts/footer.php") ?>
            