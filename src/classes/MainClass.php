<?php 

ini_set("upload_tmp_dir", "/tmp");
error_reporting(E_ALL);
ini_set('display_errors', 1);

class MainClass  
{
    
    
    function __construct()
    {
        $this->db_name = "fps_db";
        $this->db_user = "root";
        $this->db_pass = "rootpassword";
        $this->db_host = "mysql_db_container";
        try {
            $this->connectionDb = new PDO("mysql:host=$this->db_host;dbname=$this->db_name", $this->db_user, $this->db_pass,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
            
        } catch (PDOException $th) {
            echo "Error: " . $th->getMessage();
        }
    }

    function getCountrys (){
        $sql = "SELECT * FROM country";
        $stmt = $this->connectionDb->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
     
        return $result;
    }
    function getCountryid ($country) {

      
        $sql = "SELECT * FROM country WHERE country_id = '$country'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;

    }
   
    function getCountry ($country) {

      
        $sql = "SELECT * FROM country WHERE country = '$country'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;

    }
    function getLocations ($country) {

      
        $sql = "SELECT * FROM locations WHERE country_id = '$country'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;

    }

    public function getProduction ($country_id){
        $sql = "SELECT * FROM productions WHERE country_id = '$country_id'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }


    public function getTeam () {
        $sql = "SELECT * FROM team";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }
    public function getClients (){
        $sql = "SELECT * FROM clients";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function getServices ($country_id){
        $sql = "SELECT * FROM services WHERE country_id = '$country_id'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }
    
    public function getDirectors (){
        $sql = "SELECT * FROM directors";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function getWhy ($country_id) {
       
        $sql = "SELECT * FROM whys WHERE country_id = '$country_id'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;

    }

    public function getContact_Info (){
        $sql = "SELECT * FROM contact_info";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function getImages ($country, $category){

        $sql = "SELECT * FROM images WHERE country_id = '$country' AND category = '$category'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;


    }

    public function getSiteinfo () {
        $sql = "SELECT * FROM site_info WHERE id = '1'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        
        return $row;
    }

    public function getLocationsFiltre ($filtre,$country_id){
        $sql = "SELECT * FROM locations WHERE filtre_id = '$filtre' AND  country_id = '$country_id'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function getProductionsFiltre ($filtre,$film,$country_id){
        $sql = "SELECT * FROM productions WHERE filtre_id = '$filtre' AND film_tv = '$film' AND country_id = '$country_id'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }



    //admin Panel


    // Tam Section Start

    public function addTeam ($name,$photo){
     try {
        $sql = "INSERT INTO team (team_name,photo) VALUES ('$name','$photo')";
        $result = $this->connectionDb->query($sql);
        $client_id = $this->connectionDb->lastInsertId();
         return $client_id;
     } catch (PDOException $th) {
       return $th->getMessage();
     }
    }

    public function updateTam ($id,$name,$photo){
        $sql = "UPDATE team SET team_name = '$name', photo = '$photo' WHERE team_id = '$id'";
        $result = $this->connectionDb->query($sql);
        return $result;
    }

    public function deleteTam ($id){
        $sql = "DELETE FROM team WHERE team_id = '$id'";
        $result = $this->connectionDb->query($sql);
        return $result;
    }

    public function getTeamOne ($id){
        $sql = "SELECT * FROM team WHERE team_id = '$id'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }



    // Team Section End

    public function addUser ($username,$mail,$password){
        $getUserSQL = "SELECT * FROM users WHERE mail = '$mail'";
        $result = $this->connectionDb->query($getUserSQL);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        if($row){
            return false;
        }else{
            $hashPass = hash('sha256', $password);
            $sql = "INSERT INTO users (user_name,mail,user_password) VALUES ('$username','$mail','$hashPass')";
            $this->connectionDb->query($sql);
            return true;
        }
    }

    public function login ($mail,$pass){
        $hashPass = hash('sha256', $pass);
        $sql = "SELECT * FROM users WHERE mail = '$mail' AND user_password = '$hashPass'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        if($row){
            return $row;
        }else{
            return false;
        }
    }

    
    /* Clients Area Start */

    public function addClient ($client_name,$client_code,$client_country,$client_position,$client_person,$client_title,$client_comment){
       
        try {
         $sql = "INSERT INTO clients (client_name, client_country_code, client_country, client_position, client_person, client_comment_title, client_comment) VALUES 
         ('$client_name','$client_code','$client_country','$client_position','$client_person','$client_title','$client_comment')";
         $this->connectionDb->query($sql);
         $client_id = $this->connectionDb->lastInsertId();
         return $client_id;
 
        } catch (PDOException $th) {
             return $th->getMessage();
 
        }
        
     }  

    public function getOneClient ($client_id){
        $sql = "SELECT * FROM clients WHERE client_id = '$client_id'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }
    
    public function updateClient ($client_name,$client_code,$client_country,$client_position,$client_person,$client_title,$client_comment,$client_id) {
        $sql = "UPDATE clients SET client_name = '$client_name', client_country_code = '$client_code', client_country = '$client_country', client_position = '$client_position', client_person = '$client_person', client_comment_title = '$client_title', client_comment = '$client_comment' WHERE client_id = '$client_id'";
        $this->connectionDb->query($sql);
        return true;
    }
    public function deleteClient ($id){
        try {
            $sql = "DELETE FROM clients WHERE client_id = '$id'";
            $this->connectionDb->query($sql);
            return true;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    
    }
    /* Client Area End */

    public function AddCountry ($images,$country,$code){
       try {
        $sql = "INSERT INTO country (country,country_slides,country_code) VALUES ('$country','$images','$code')";
        $this->connectionDb->query($sql);
        $country_id = $this->connectionDb->lastInsertId();
        return $country_id;
       } catch (PDOException $th) {
        return $th->getMessage();
       }
        
    }
    
    /* Director Area Start */
    public function addDirectors ($name,$email,$surname,$position,$image,$about,$aboutimage){
        try {
           
            $sql = "INSERT INTO directors (name,email,surname,position,image,about,about_image) VALUES ('$name','$email','$surname','$position','$image', '$about','$aboutimage')";
            $this->connectionDb->query($sql);
            $director_id = $this->connectionDb->lastInsertId();
            return $director_id;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }

   public function getOneDirector ($director_id){
        $sql = "SELECT * FROM directors WHERE director_id = '$director_id'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function deleteDirector ($director_id){
        try {
            $sql = "DELETE FROM directors WHERE director_id = '$director_id'";
            $this->connectionDb->query($sql);
            return true;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }

    public function updateDirector ($name,$email,$surname,$position,$image,$director_id,$about,$aboutimage){
        try {
            $sql = "UPDATE directors SET name = '$name', email = '$email', surname = '$surname', position = '$position', image = '$image', about = '$about', about_image = '$aboutimage' WHERE director_id = '$director_id'";
            $this->connectionDb->query($sql);
            return true;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }

    /* Director Area End */

     /* Location Area Start */

    public function addLocation ($location,$location_photo,$country_id,$filtre){
        try {
            $sql = "INSERT INTO locations (location,location_photo,country_id,filtre_id) VALUES ('$location','$location_photo','$country_id','$filtre')";
            $this->connectionDb->query($sql);
            $location_id = $this->connectionDb->lastInsertId();
            return $location_id;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }

    public function getAllLocation (){
        $sql = "SELECT * FROM locations";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function getOneLocation ($location_id){
        $sql = "SELECT * FROM locations WHERE location_id = '$location_id'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function deleteLocation ($location_id){
        try {
            $sql = "DELETE FROM locations WHERE location_id = '$location_id'";
            $this->connectionDb->query($sql);
            return true;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }

    public function updateLocation ($location,$location_photo,$country_id,$location_id,$filtre){
        try {
            $sql = "UPDATE locations SET  location_photo = '$location_photo', location = '$location', country_id = '$country_id', filtre_id = '$filtre',  WHERE location_id = '$location_id'";
            return $this->connectionDb->query($sql);
            
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }

    /* Location Area End */

    /*Production Area Start */

    public function addProduction ($production,$production_photo,$country_id,$filtre,$isVideo,$videolink,$feature,$person,$film,$behind,$comment){
        try {
            $sql = "INSERT INTO productions (production_title,production_photo,country_id,filtre_id,videoLink,isVideo,isFeatured,persons,film_tv,behind,comment) VALUES ('$production','$production_photo','$country_id','$filtre','$videolink','$isVideo', '$feature','$person','$film','$behind','$comment')";  
            $this->connectionDb->query($sql);
            $production_id = $this->connectionDb->lastInsertId();
            return $production_id;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }

    public function getProductionAll (){
        $sql = "SELECT * FROM productions";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function getProductionOne ($production_id){
        $sql = "SELECT * FROM productions WHERE production_id = '$production_id'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function deleteProduction ($production_id){
        try {
            $sql = "DELETE FROM productions WHERE production_id = '$production_id'";
            $this->connectionDb->query($sql);
            return true;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }

    public function updateProduction ($production,$production_photo,$country_id,$production_id,$filtre,$isVideo,$videoLink,$feature,$person,$film,$behind,$comment){
        try {
            $sql = "UPDATE productions SET production_title = '$production', production_photo = '$production_photo', country_id = '$country_id', isVideo = '$isVideo', videoLink = '$videoLink', filtre_id = '$filtre', isFeatured = '$feature', persons = '$person',  film_tv = $film, behind = '$behind', comment = '$comment' WHERE production_id = '$production_id'";
            $this->connectionDb->query($sql);
            return true;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }

    /* Production Area End */

    /* Services Area Start */

    public function addServices ($service,$service_detail,$country_id){
        try {
            $sql = "INSERT INTO services (service_title,service_detail,country_id) VALUES ('$service','$service_detail','$country_id')";
            $this->connectionDb->query($sql);
            $service_id = $this->connectionDb->lastInsertId();
            return $service_id;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }

    public function getServicesAll (){
        $sql = "SELECT * FROM services";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function getServicesOne ($service_id){
        $sql = "SELECT * FROM services WHERE service_id = '$service_id'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function deleteServices ($service_id){
        try {
            $sql = "DELETE FROM services WHERE service_id = '$service_id'";
            $this->connectionDb->query($sql);
            return true;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }

    public function updateServices ($service,$service_detail,$country_id,$service_id){
        try {
            $sql = "UPDATE services SET service_title = '$service', service_detail = '$service_detail', country_id = '$country_id' WHERE service_id = '$service_id'";
            $this->connectionDb->query($sql);
            return true;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }

    /* Services Area End */

   

    /* WHy Area Start */

    public function addWhy ($why,$country_id){
        try {
            $sql = "INSERT INTO whys (why_title,country_id) VALUES ('$why','$country_id')";
            $this->connectionDb->query($sql);
            $why_id = $this->connectionDb->lastInsertId();
            return $why_id;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }

    public function getWhyAll (){
        $sql = "SELECT * FROM whys";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function getWhyOne ($why_id){
        $sql = "SELECT * FROM whys WHERE why_id = '$why_id'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function deleteWhy ($why_id){
        try {
            $sql = "DELETE FROM whys WHERE why_id = '$why_id'";
            $this->connectionDb->query($sql);
            return true;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }

    public function updateWhy ($why,$country_id,$why_id){
        try {
            $sql = "UPDATE whys SET why_title = '$why', country_id = '$country_id' WHERE why_id = '$why_id'";
            $this->connectionDb->query($sql);
            return true;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }


    /* Why Area End */

    /* Contact Area Start */

    public function addContact ($title, $description, $address,$phone,$map){
        try {
            $sql = "INSERT INTO contact_info (title,description,address,phone,map) VALUES ('$title','$description','$address','$phone','$map')";
            $this->connectionDb->query($sql);
            $contact_id = $this->connectionDb->lastInsertId();
            return $contact_id;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }

    public function getContactAll (){
        $sql = "SELECT * FROM contact_info";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function getContactOne ($contact_id){
        $sql = "SELECT * FROM contact_info WHERE contact_id = '$contact_id'";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function deleteContact ($contact_id){
        try {
            $sql = "DELETE FROM contact_info WHERE contact_id = '$contact_id'";
            $this->connectionDb->query($sql);
            return true;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }

    public function updateContact ($title, $description, $address,$phone,$map,$contact_id){
        try {
            $sql = "UPDATE contact_info SET title = '$title', description = '$description', address = '$address', phone = '$phone', map = '$map' WHERE contact_id = '$contact_id'";
            $this->connectionDb->query($sql);
            return true;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }

    /* Contact Area End */

    function updateSiteInfo ($facebook,$twitter, $instagram, $desc, $about,$title,$title_sub,$menusub,$site_title) {

        try {
            $sql = "UPDATE site_info SET facebook = '$facebook', twitter = '$twitter', instagram = '$instagram', desc = '$desc', about = '$about', index_title_big = '$title', index_title_sub = '$title_sub', menu_subtitle = '$menusub', title = '$site_title' WHERE id = 1";
            //echo "<br>".$sql;
            $this->connectionDb->query($sql);
            return $this->connectionDb->lastInsertId();
        } catch (PDOException $th) {
            return $th->getMessage();
        }
        

    }

    public function deleteCountry($id){
        try {
            $sql = "DELETE FROM country WHERE country_id = '$id'";
            $this->connectionDb->query($sql);
            return true;
        } catch (PDOException $th) {
            return $th->getMessage();
        }
    }
  
   

}
