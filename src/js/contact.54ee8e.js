(function (d) {
    var h = [];
    d.loadImages = function (a, e) {
        "string" == typeof a && (a = [a]);
        for (var f = a.length, g = 0, b = 0; b < f; b++) {
            var c = document.createElement("img");
            c.onload = function () {
                g++;
                g == f && d.isFunction(e) && e()
            };
            c.src = a[b];
            h.push(c)
        }
    }
})(window.jQuery);
var wl;

var lwi = -1;

function thresholdPassed() {
    var w = $(window).width();
    var p = false;
    var cw = 0;
    if (w >= 480) {
        cw++;
    }
    if (w >= 960) {
        cw++;
    }
    if (w >= 1440) {
        cw++;
    }
    if (w >= 1920) {
        cw++;
    }
    if (lwi != cw) {
        p = true;
    }
    lwi = cw;
    return p;
}

function em1() {
    var c = "bAgqtqspevdujpo/dpn";
    var addr = "mailto:";
    for (var i = 0; i < c.length; i++) addr += String.fromCharCode(c.charCodeAt(i) - 1);
    window.location.href = addr;
}

function em2() {
    var c = "kbnftAsjwbmtdippmqjduvsft/dpn";
    var addr = "mailto:";
    for (var i = 0; i < c.length; i++) addr += String.fromCharCode(c.charCodeAt(i) - 1);
    window.location.href = addr;
}

function em3() {
    var c = "jtjmbzAgqtqspevdujpo/dpn";
    var addr = "mailto:";
    for (var i = 0; i < c.length; i++) addr += String.fromCharCode(c.charCodeAt(i) - 1);
    window.location.href = addr;
}

function em4() {
    var c = "fxbAgqtqspevdujpo/dpn";
    var addr = "mailto:";
    for (var i = 0; i < c.length; i++) addr += String.fromCharCode(c.charCodeAt(i) - 1);
    window.location.href = addr;
}! function () {
    if ("Promise" in window && void 0 !== window.performance) {
        var e, t, r = document,
            n = function () {
                return r.createElement("link")
            },
            o = new Set,
            a = n(),
            i = a.relList && a.relList.supports && a.relList.supports("prefetch"),
            s = location.href.replace(/#[^#]+$/, "");
        o.add(s);
        var c = function (e) {
                var t = location,
                    r = "http:",
                    n = "https:";
                if (e && e.href && e.origin == t.origin && [r, n].includes(e.protocol) && (e.protocol != r || t.protocol != n)) {
                    var o = e.pathname;
                    if (!(e.hash && o + e.search == t.pathname + t.search || "?preload=no" == e.search.substr(-11) || ".html" != o.substr(-5) && ".html" != o.substr(-5) && "/" != o.substr(-1))) return !0
                }
            },
            u = function (e) {
                var t = e.replace(/#[^#]+$/, "");
                if (!o.has(t)) {
                    if (i) {
                        var a = n();
                        a.rel = "prefetch", a.href = t, r.head.appendChild(a)
                    } else {
                        var s = new XMLHttpRequest;
                        s.open("GET", t, s.withCredentials = !0), s.send()
                    }
                    o.add(t)
                }
            },
            p = function (e) {
                return e.target.closest("a")
            },
            f = function (t) {
                var r = t.relatedTarget;
                r && p(t) == r.closest("a") || e && (clearTimeout(e), e = void 0)
            },
            d = {
                capture: !0,
                passive: !0
            };
        r.addEventListener("touchstart", function (e) {
            t = performance.now();
            var r = p(e);
            c(r) && u(r.href)
        }, d), r.addEventListener("mouseover", function (r) {
            if (!(performance.now() - t < 1200)) {
                var n = p(r);
                c(n) && (n.addEventListener("mouseout", f, {
                    passive: !0
                }), e = setTimeout(function () {
                    u(n.href), e = void 0
                }, 80))
            }
        }, d)
    }
}();

loadGoogleMaps = function () {
    {
        var mapOptions = {
            zoom: 14,
            center: new google.maps.LatLng(41.051299, 29.0524111),
            mapTypeId: google.maps.MapTypeId.HYBRID,
            styles: [{
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "water",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#2f343b"
                }]
            }, {
                "featureType": "landscape",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#703030"
                }]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#2f343b"
                }, {
                    "weight": 1
                }]
            }]
        };
        var map = new google.maps.Map($('.un331').get(0), mapOptions);
        var marker = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(41.051299, 29.0524111),
        });
    } {
        var mapOptions = {
            zoom: 14,
            center: new google.maps.LatLng(40.7202344, -73.99863569999999),
            mapTypeId: google.maps.MapTypeId.HYBRID,
            styles: [{
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "water",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#2f343b"
                }]
            }, {
                "featureType": "landscape",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#703030"
                }]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#2f343b"
                }, {
                    "weight": 1
                }]
            }]
        };
        var map = new google.maps.Map($('.un332').get(0), mapOptions);
        var marker = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(40.7202344, -73.99863569999999),
        });
    } {
        var mapOptions = {
            zoom: 14,
            center: new google.maps.LatLng(40.367613, 49.836567),
            mapTypeId: google.maps.MapTypeId.HYBRID,
            styles: [{
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "water",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#2f343b"
                }]
            }, {
                "featureType": "landscape",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#703030"
                }]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#2f343b"
                }, {
                    "weight": 1
                }]
            }]
        };
        var map = new google.maps.Map($('.un333').get(0), mapOptions);
        var marker = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(40.367613, 49.836567),
        });
    } {
        var mapOptions = {
            zoom: 14,
            center: new google.maps.LatLng(41.7151377, 44.827096),
            mapTypeId: google.maps.MapTypeId.HYBRID,
            styles: [{
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "water",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#2f343b"
                }]
            }, {
                "featureType": "landscape",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#703030"
                }]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#2f343b"
                }, {
                    "weight": 1
                }]
            }]
        };
        var map = new google.maps.Map($('.un334').get(0), mapOptions);
        var marker = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(41.7151377, 44.827096),
        });
    }
};
$(function () {
    ! function () {
        var e = document.querySelectorAll('a[href^="#"]');
        [].forEach.call(e, function (e) {
            e.addEventListener("click", function (t) {
                var o = 0;
                if (e.hash.length > 1) {
                    var l = parseFloat(getComputedStyle(document.body).getPropertyValue("zoom"));
                    l || (l = 1);
                    var n = document.querySelectorAll('[name="' + e.hash.slice(1) + '"]')[0];
                    o = (n.getBoundingClientRect().top + pageYOffset) * l
                }
                if ("scrollBehavior" in document.documentElement.style) scroll({
                    top: o,
                    left: 0,
                    behavior: "smooth"
                });
                else if ("requestAnimationFrame" in window) {
                    var r = pageYOffset,
                        a = null;
                    requestAnimationFrame(function e(t) {
                        a || (a = t);
                        var l = t - a;
                        scrollTo(0, r < o ? (o - r) * l / 400 + r : r - (r - o) * l / 400), l < 400 ? requestAnimationFrame(e) : scrollTo(0, o)
                    })
                } else scrollTo(0, o);
                t.preventDefault()
            }, !1)
        })
    }();
    setTimeout(function () {
        pop.openPopup('popup19');
    }, 0);
    setInterval(function () {
        pop.openPopup('popup21');
    }, 0);
    setTimeout(function () {
        pop.openPopup('popup22');
    }, 0);

    wl = new woolite();
    wl.init();
    wl.addAnimation($('.un313'), "1.00s", "0.50s", 1, 100);
    wl.addManualAnimation($('.un311'), "0.50s", "0.00s");
    wl.start();
    if (location.hash) {
        var e = location.hash.replace("#", ""),
            o = function () {
                var t = document.querySelectorAll('[name="' + e + '"]')[0];
                t && t.scrollIntoView(), "complete" != document.readyState && setTimeout(o, 100)
            };
        o()
    }

});