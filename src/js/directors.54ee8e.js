(function(d){var h=[];d.loadImages=function(a,e){"string"==typeof a&&(a=[a]);for(var f=a.length,g=0,b=0;b<f;b++){var c=document.createElement("img");c.onload=function(){g++;g==f&&d.isFunction(e)&&e()};c.src=a[b];h.push(c)}}})(window.jQuery);
var wl;

var lwi=-1;function thresholdPassed(){var w=$(window).width();var p=false;var cw=0;if(w>=480){cw++;}if(w>=960){cw++;}if(w>=1440){cw++;}if(w>=1920){cw++;}if(lwi!=cw){p=true;}lwi=cw;return p;}
!function(){if("Promise"in window&&void 0!==window.performance){var e,t,r=document,n=function(){return r.createElement("link")},o=new Set,a=n(),i=a.relList&&a.relList.supports&&a.relList.supports("prefetch"),s=location.href.replace(/#[^#]+$/,"");o.add(s);var c=function(e){var t=location,r="http:",n="https:";if(e&&e.href&&e.origin==t.origin&&[r,n].includes(e.protocol)&&(e.protocol!=r||t.protocol!=n)){var o=e.pathname;if(!(e.hash&&o+e.search==t.pathname+t.search||"?preload=no"==e.search.substr(-11)||".html"!=o.substr(-5)&&".html"!=o.substr(-5)&&"/"!=o.substr(-1)))return!0}},u=function(e){var t=e.replace(/#[^#]+$/,"");if(!o.has(t)){if(i){var a=n();a.rel="prefetch",a.href=t,r.head.appendChild(a)}else{var s=new XMLHttpRequest;s.open("GET",t,s.withCredentials=!0),s.send()}o.add(t)}},p=function(e){return e.target.closest("a")},f=function(t){var r=t.relatedTarget;r&&p(t)==r.closest("a")||e&&(clearTimeout(e),e=void 0)},d={capture:!0,passive:!0};r.addEventListener("touchstart",function(e){t=performance.now();var r=p(e);c(r)&&u(r.href)},d),r.addEventListener("mouseover",function(r){if(!(performance.now()-t<1200)){var n=p(r);c(n)&&(n.addEventListener("mouseout",f,{passive:!0}),e=setTimeout(function(){u(n.href),e=void 0},80))}},d)}}();

$(function(){
r=function(){if(thresholdPassed()){dpi=window.devicePixelRatio;if($(window).width()>=1920){var e=document.querySelector('.un180');e.setAttribute('src',(dpi>1)?'images/alfonso-castro-cgqg8bx2kbw-unsplash-742.jpg':'images/alfonso-castro-cgqg8bx2kbw-unsplash-371.jpg');
var e=document.querySelector('.un181');e.setAttribute('src',(dpi>1)?'images/metin-ozer-ismtwuktndo-unsplash-740.jpg':'images/metin-ozer-ismtwuktndo-unsplash-370.jpg');
var e=document.querySelector('.un182');e.setAttribute('src',(dpi>1)?'images/tim-mossholder-l1r4qsvjmom-unsplash-742.jpg':'images/tim-mossholder-l1r4qsvjmom-unsplash-371.jpg');
var e=document.querySelector('.un183');e.setAttribute('src',(dpi>1)?'images/ayo-ogunseinde-uqt55tgbqzi-unsplash-740.jpg':'images/ayo-ogunseinde-uqt55tgbqzi-unsplash-370.jpg');
var e=document.querySelector('.un184');e.setAttribute('src',(dpi>1)?'images/derek-story-jhc51kbvcg0-unsplash-742.jpg':'images/derek-story-jhc51kbvcg0-unsplash-371.jpg');
var e=document.querySelector('.un185');e.setAttribute('src',(dpi>1)?'images/angelo-abear-xdevbdyfmd8-unsplash-742.jpg':'images/angelo-abear-xdevbdyfmd8-unsplash-371.jpg');
var e=document.querySelector('.un186');e.setAttribute('src',(dpi>1)?'images/ludvig-wiese-d-mfhm-jhwc-unsplash-742.jpg':'images/ludvig-wiese-d-mfhm-jhwc-unsplash-371.jpg');
var e=document.querySelector('.un187');e.setAttribute('src',(dpi>1)?'images/shalom-de-leon-qsqkwk09tui-unsplash-740.jpg':'images/shalom-de-leon-qsqkwk09tui-unsplash-370.jpg');}else if($(window).width()>=1440){var e=document.querySelector('.un180');e.setAttribute('src',(dpi>1)?'images/alfonso-castro-cgqg8bx2kbw-unsplash-556.jpg':'images/alfonso-castro-cgqg8bx2kbw-unsplash-278.jpg');
var e=document.querySelector('.un181');e.setAttribute('src',(dpi>1)?'images/metin-ozer-ismtwuktndo-unsplash-556.jpg':'images/metin-ozer-ismtwuktndo-unsplash-278.jpg');
var e=document.querySelector('.un182');e.setAttribute('src',(dpi>1)?'images/tim-mossholder-l1r4qsvjmom-unsplash-556.jpg':'images/tim-mossholder-l1r4qsvjmom-unsplash-278.jpg');
var e=document.querySelector('.un183');e.setAttribute('src',(dpi>1)?'images/ayo-ogunseinde-uqt55tgbqzi-unsplash-556.jpg':'images/ayo-ogunseinde-uqt55tgbqzi-unsplash-278.jpg');
var e=document.querySelector('.un184');e.setAttribute('src',(dpi>1)?'images/derek-story-jhc51kbvcg0-unsplash-556.jpg':'images/derek-story-jhc51kbvcg0-unsplash-278.jpg');
var e=document.querySelector('.un185');e.setAttribute('src',(dpi>1)?'images/angelo-abear-xdevbdyfmd8-unsplash-556.jpg':'images/angelo-abear-xdevbdyfmd8-unsplash-278.jpg');
var e=document.querySelector('.un186');e.setAttribute('src',(dpi>1)?'images/ludvig-wiese-d-mfhm-jhwc-unsplash-556.jpg':'images/ludvig-wiese-d-mfhm-jhwc-unsplash-278.jpg');
var e=document.querySelector('.un187');e.setAttribute('src',(dpi>1)?'images/shalom-de-leon-qsqkwk09tui-unsplash-556.jpg':'images/shalom-de-leon-qsqkwk09tui-unsplash-278.jpg');}else if($(window).width()>=960){var e=document.querySelector('.un180');e.setAttribute('src',(dpi>1)?'images/alfonso-castro-cgqg8bx2kbw-unsplash-370.jpg':'images/alfonso-castro-cgqg8bx2kbw-unsplash-185.jpg');
var e=document.querySelector('.un181');e.setAttribute('src',(dpi>1)?'images/metin-ozer-ismtwuktndo-unsplash-372.jpg':'images/metin-ozer-ismtwuktndo-unsplash-186.jpg');
var e=document.querySelector('.un182');e.setAttribute('src',(dpi>1)?'images/tim-mossholder-l1r4qsvjmom-unsplash-370.jpg':'images/tim-mossholder-l1r4qsvjmom-unsplash-185.jpg');
var e=document.querySelector('.un183');e.setAttribute('src',(dpi>1)?'images/ayo-ogunseinde-uqt55tgbqzi-unsplash-372.jpg':'images/ayo-ogunseinde-uqt55tgbqzi-unsplash-186.jpg');
var e=document.querySelector('.un184');e.setAttribute('src',(dpi>1)?'images/derek-story-jhc51kbvcg0-unsplash-370.jpg':'images/derek-story-jhc51kbvcg0-unsplash-185.jpg');
var e=document.querySelector('.un185');e.setAttribute('src',(dpi>1)?'images/angelo-abear-xdevbdyfmd8-unsplash-370.jpg':'images/angelo-abear-xdevbdyfmd8-unsplash-185.jpg');
var e=document.querySelector('.un186');e.setAttribute('src',(dpi>1)?'images/ludvig-wiese-d-mfhm-jhwc-unsplash-370.jpg':'images/ludvig-wiese-d-mfhm-jhwc-unsplash-185.jpg');
var e=document.querySelector('.un187');e.setAttribute('src',(dpi>1)?'images/shalom-de-leon-qsqkwk09tui-unsplash-372.jpg':'images/shalom-de-leon-qsqkwk09tui-unsplash-186.jpg');}else if($(window).width()>=480){var e=document.querySelector('.un180');e.setAttribute('src',(dpi>1)?'images/alfonso-castro-cgqg8bx2kbw-unsplash-420-1.jpg':'images/alfonso-castro-cgqg8bx2kbw-unsplash-210-1.jpg');
var e=document.querySelector('.un181');e.setAttribute('src',(dpi>1)?'images/metin-ozer-ismtwuktndo-unsplash-420-1.jpg':'images/metin-ozer-ismtwuktndo-unsplash-210-1.jpg');
var e=document.querySelector('.un182');e.setAttribute('src',(dpi>1)?'images/tim-mossholder-l1r4qsvjmom-unsplash-420-1.jpg':'images/tim-mossholder-l1r4qsvjmom-unsplash-210-1.jpg');
var e=document.querySelector('.un183');e.setAttribute('src',(dpi>1)?'images/ayo-ogunseinde-uqt55tgbqzi-unsplash-420-1.jpg':'images/ayo-ogunseinde-uqt55tgbqzi-unsplash-210-1.jpg');
var e=document.querySelector('.un184');e.setAttribute('src',(dpi>1)?'images/derek-story-jhc51kbvcg0-unsplash-420-1.jpg':'images/derek-story-jhc51kbvcg0-unsplash-210-1.jpg');
var e=document.querySelector('.un185');e.setAttribute('src',(dpi>1)?'images/angelo-abear-xdevbdyfmd8-unsplash-420-1.jpg':'images/angelo-abear-xdevbdyfmd8-unsplash-210-1.jpg');
var e=document.querySelector('.un186');e.setAttribute('src',(dpi>1)?'images/ludvig-wiese-d-mfhm-jhwc-unsplash-420-1.jpg':'images/ludvig-wiese-d-mfhm-jhwc-unsplash-210-1.jpg');
var e=document.querySelector('.un187');e.setAttribute('src',(dpi>1)?'images/shalom-de-leon-qsqkwk09tui-unsplash-420-1.jpg':'images/shalom-de-leon-qsqkwk09tui-unsplash-210-1.jpg');}else{var e=document.querySelector('.un180');e.setAttribute('src',(dpi>1)?'images/alfonso-castro-cgqg8bx2kbw-unsplash-280-1.jpg':'images/alfonso-castro-cgqg8bx2kbw-unsplash-140-1.jpg');
var e=document.querySelector('.un181');e.setAttribute('src',(dpi>1)?'images/metin-ozer-ismtwuktndo-unsplash-280-1.jpg':'images/metin-ozer-ismtwuktndo-unsplash-140-1.jpg');
var e=document.querySelector('.un182');e.setAttribute('src',(dpi>1)?'images/tim-mossholder-l1r4qsvjmom-unsplash-280-1.jpg':'images/tim-mossholder-l1r4qsvjmom-unsplash-140-1.jpg');
var e=document.querySelector('.un183');e.setAttribute('src',(dpi>1)?'images/ayo-ogunseinde-uqt55tgbqzi-unsplash-280-1.jpg':'images/ayo-ogunseinde-uqt55tgbqzi-unsplash-140-1.jpg');
var e=document.querySelector('.un184');e.setAttribute('src',(dpi>1)?'images/derek-story-jhc51kbvcg0-unsplash-280-1.jpg':'images/derek-story-jhc51kbvcg0-unsplash-140-1.jpg');
var e=document.querySelector('.un185');e.setAttribute('src',(dpi>1)?'images/angelo-abear-xdevbdyfmd8-unsplash-280-1.jpg':'images/angelo-abear-xdevbdyfmd8-unsplash-140-1.jpg');
var e=document.querySelector('.un186');e.setAttribute('src',(dpi>1)?'images/ludvig-wiese-d-mfhm-jhwc-unsplash-280-1.jpg':'images/ludvig-wiese-d-mfhm-jhwc-unsplash-140-1.jpg');
var e=document.querySelector('.un187');e.setAttribute('src',(dpi>1)?'images/shalom-de-leon-qsqkwk09tui-unsplash-280-1.jpg':'images/shalom-de-leon-qsqkwk09tui-unsplash-140-1.jpg');}}};
if(!window.HTMLPictureElement){$(window).resize(r);r();}
!function(){var e=document.querySelectorAll('a[href^="#"]');[].forEach.call(e,function(e){e.addEventListener("click",function(t){var o=0;if(e.hash.length>1){var l=parseFloat(getComputedStyle(document.body).getPropertyValue("zoom"));l||(l=1);var n=document.querySelectorAll('[name="'+e.hash.slice(1)+'"]')[0];o=(n.getBoundingClientRect().top+pageYOffset)*l}if("scrollBehavior"in document.documentElement.style)scroll({top:o,left:0,behavior:"smooth"});else if("requestAnimationFrame"in window){var r=pageYOffset,a=null;requestAnimationFrame(function e(t){a||(a=t);var l=t-a;scrollTo(0,r<o?(o-r)*l/400+r:r-(r-o)*l/400),l<400?requestAnimationFrame(e):scrollTo(0,o)})}else scrollTo(0,o);t.preventDefault()},!1)})}();
setTimeout(function(){pop.openPopup('popup9');},0);setInterval(function(){pop.openPopup('popup11');},0);setTimeout(function(){pop.openPopup('popup12');},0);wl=new woolite();
wl.init();
wl.addAnimation($('.un190'), "1.00s", "0.50s", 1, 100);
wl.addManualAnimation($('.un188'), "0.50s", "0.00s");
wl.start();
if(location.hash){var e=location.hash.replace("#",""),o=function(){var t=document.querySelectorAll('[name="'+e+'"]')[0];t&&t.scrollIntoView(),"complete"!=document.readyState&&setTimeout(o,100)};o()}

});