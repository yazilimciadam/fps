<?php 

include "classes/MainClass.php";
$veritabani = new MainClass();
$countrys = $veritabani->getCountrys();
$site_info = $veritabani->getSiteinfo();
//$country = $veritabani->getCountry($_GET["country"]);
$production = $veritabani->getProductionOne($_GET["production_id"]);
$country = $veritabani->getCountryid($production["country_id"]);
$comment = json_decode($production["comment"]);
$behinds = json_decode($production["behind"]);


?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title><?php echo $site_info["title"] ?> |  <?php echo $production["production_title"] ?></title>
  <meta name="referrer" content="origin-when-cross-origin">
  <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
  <style>
    .anim {
      visibility: hidden
    }
  </style>
 <link
  rel="stylesheet"
  href="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css"
/>

<script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>
  <script>!function () { var A = new Image; A.onload = A.onerror = function () { 1 != A.height && document.body.classList.remove("webp") }, A.src = "data:image/webp;base64,UklGRiQAAABXRUJQVlA4IBgAAAAwAQCdASoBAAEAD8D+JaQAA3AA/ua1AAA" }();
  </script>
  <link rel="stylesheet" href="css/site.54ee8e.css" type="text/css">
  <link href="https://vjs.zencdn.net/7.20.1/video-js.css" rel="stylesheet" />
</head>

<body class="webp" id="b1">
  <div class="v8 ps320 s407 c488">
    <div class="ps279">
    </div>
    <div class="v2 ps1 s408 c489">
      <div class="ps321 v8 s342">
        <div id="other" class="v3 ps281 s343 c490">
          <p class="p6"><span class="f24"><a href="/Country/<?php echo $country["country"] ?>">Turkey</a></span><span class="f24"> </span></p>
        </div>
      </div>
      <div class="v3 ps322 s409 c491">
        <div class="ps323 v8 s410">
          <div class="v3 ps281 s411 c441">
            <div id="commercials-altmenu" class="v3 ps324 s412 c492">
              <p class="p6"><span class="f80"><a href="#"></a></span><span class="f81">
                  <?php  echo $production["production_title"] ?></span></p>
            </div>
            <div class="v3 ps325 s413 c493">
              <div class="v2 ps2 s414 c1">
                <div class="v2 ps2 s415 c208">
                <a onclick="pop&&pop.openPopup('popup36');return false" style="cursor:pointer;" href="javascript:void(0)"
              id="bts" class="f82 btn28 v7 s416">READ THE COMMENTS</a>
                </div>
                <div class="v2 ps326 s417 c207">
                <a onclick="pop&&pop.openPopup('popup400');return false" style="cursor:pointer;" href="javascript:void(0)"
              id="bts" class="f82 btn29 v7 s418">BEHIND THE SCENES</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="ps327 v1 s146">
    <div class="v2 ps3 s3 c421">
      <div class="wrapper9">
        <style>
          ::selection {
            color: white;
            background: #ff7200;
          }

          #menu a {
            transition: all 0.5s ease;
          }

          #menu a:hover {
            transition: all 0.5s ease;
            color: #ffffff;
          }

          #earth a {
            transition: all 0.5s ease;
          }

          #earth a:hover {
            transition: all 0.5s ease;
            color: #FF6700;
          }

          #hero a {
            transition: all 0.5s ease;
          }

          #hero a:hover {
            transition: all 0.5s ease;
            color: #ffffff;
          }

          #locations-folders {
            transition: all 0.5s ease;
            opacity: 0.8;
          }

          #locations-folders:hover {
            transition: all 0.5s ease;
            transform: translateY(-10px) scale(1.1);
            opacity: 1;
          }

          #overlay {
            transition: all 0.5s ease;
            opacity: 0;
            transform: scale(0.8);
          }

          #overlay:hover {
            transition: all 0.5s ease;
            opacity: 0.7;
            transform: scale(1);
          }

          #locations-gallery {
            transition: all 0.5s ease;
            opacity: 1;
          }

          #locations-gallery:hover {
            transition: all 0.5s ease;
            transform: translateY(-5px) scale(1);
            opacity: 1;
          }

          #locations-gallery a:hover {
            color: #FF6700;
          }

          #locations-link {
            opacity: 0;
          }

          #locations-link:hover {
            opacity: 1;
          }

          #other a:hover {
            color: #FF6700;
            transition: all 0.5s ease;
          }

          #product p {
            transition: all 0.5s ease;
          }

          #product p:hover {
            transition: all 0.5s ease;
            color: #ffffff;
          }

          #bigbaslik a {
            transition: all 0.5s ease;
          }

          #bigbaslik a:hover {
            color: #FF6700;
            transition: all 0.5s ease;
          }

          #commercials-altmenu a {
            transition: all 0.5s ease;
          }

          #commercials-altmenu a:hover {
            transition: all 0.5s ease;
            color: #FF6700;
          }

          #commercial-link a {
            transition: all 0.5s ease;
          }

          #commercial-link a:hover {
            transition: all 0.5s ease;
            color: #FF6700;
          }

          #about {
            transition: all 0.5s ease;
            opacity: 0.4;
          }

          #about:hover {
            transition: all 0.5s ease;
            transform: translateY(-10px);
            opacity: 1;
          }

          #bts {
            transition: all 0.5s ease;
            transform: scale(1);
          }

          #bts:hover {
            transition: all 0.5s ease;
            transform: scale(1.1);
          }

          #directors {
            transition: all 0.5s ease;
            opacity: 0.5;
          }

          #directors:hover {
            transition: all 0.5s ease;
            transform: translateY(-10px);
            opacity: 1;
          }
        </style>
      </div>
    </div>
  </div>
  <div class="v4 ps30 s419 c494">
    <div class="v2 ps1 s408 c495">
      <div class="ps328 v9 s420">
        <div id="other" class="v4 ps300 s421 c496">
          <p class="p4 f70"><a href="/Country/<?php echo $country["country"] ?>"><?php echo $country["country"] ?></a><span class="f70">&nbsp;</span></p>
          <p class="p4 f83"></p>
        </div>
      </div>
      <div class="v4 ps329 s422 c497">
        <div class="ps330 v9 s423">
          <div id="commercials-altmenu" class="v4 ps300 s424 c498">
            <p class="p4"><span class="f84"><a href="#"></a></span><span class="f85">
                <?php $production["production_title"] ?></span></p>
          </div>
        </div>
      </div>
    </div>
    <div class="ps331 v1 s425">
      <div class="v2 ps332 s414 c254">
        <div class="v2 ps2 s414 c1">
          <div class="v2 ps2 s415 c141">
            <a onclick="pop&&pop.openPopup('popup36');return false" style="cursor:pointer;" href="javascript:void(0)"
              id="bts" class="f82 btn28 v7 s416">READ THE COMMENTS</a>
          </div>
          <div class="v2 ps326 s417 c104">
            <a onclick="pop&&pop.openPopup('popup37');return false" style="cursor:pointer;" href="javascript:void(0)"
              id="bts" class="f82 btn29 v7 s418">BEHIND THE SCENES</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="ps333 v1 s426">
    <div class="un725 v2 ps264 s427 c476">
    <video id="my-video"
    class="video-js"
    style="width:100%; height:100%;"
    controls
    preload="auto"
    width="auto"
    height="auto"
    poster="<?php echo $production["production_photo"] ?>"
    data-setup="{}" 
       class="un729">
       <source src="<?php echo $production["videoLink"] ?>" type="video/mp4" />
            </video>
    </div>
    <div class="v2 ps334 s428 c57">
      <div class="v2 ps2 s428 c1">
        <div class="v2 ps2 s429 c499">
          <?php 
            $data = json_decode($production["persons"],JSON_UNESCAPED_UNICODE);
            
            foreach ($data as $person => $key) {
             
            
          ?>
          <p class="p2 f86"><?php echo $person ?>: <span class="f87"><?php echo $key ?></span>&nbsp;</p>
          <?php } ?>
        </div>
       
      </div>
    </div>
  </div>
  <div class="v2 ps337 s168 c502"></div>
  <div id="popup38" style="transform:translate3d(-999999px,0,0)" data-popup-type="2" data-popup-group="7" class="ps143">
    <div onclick="event.stopPropagation()" class="popup v5 ps29 s26 c503">
      <div class="v4 ps30 s27 c38">
        <div class="v4 ps31 s28 c504"></div>
        <div class="v4 ps32 s29 c505"></div>
      </div>
    </div>
  </div>
  <div id="popup36" style="transform:translate3d(-999999px,0,0)" data-popup-type="1" class="popup ps338"
    onclick="pop.closePopup('popup36')">
    <div class="ps145">
      <div class="animm bounceIn un726 modalpopup v6 ps339 s432 c506" onclick="event.stopPropagation()">
        <div class="c507">
          <div class="v2 ps340 s433 c508">
            <p class="p2 f88"><?php echo $comment->title ?></p>
          </div>
          <div class="v2 ps341 s434 c509">
            <p class="p2 f89"> <?php echo $comment->comment ?></p>
          </div>
          <div class="v2 ps342 s435 c510">
            <p class="p2 f90"><?php echo $comment->person ?>, <?php echo $comment->position ?></p>
          </div>
          <div class="v2 ps343 s436 c511">
            <p class="p2 f91"><?php echo $comment->firm?></p>
           
          </div>
          <div class="v2 ps344 s437 c512"></div>
        </div>
      </div>
    </div>
  </div>
  <div id="popup37" style="transform:translate3d(-999999px,0,0)" data-popup-type="1" class="popup ps144"
    onclick="pop.closePopup('popup37')">
    <div class="ps145">
      <div class="animm bounceIn un727 modalpopup v6 ps345 s438 c513" onclick="event.stopPropagation()">
        <div class="c514">
          <div class="un730 v2 ps2 s439 c1">
            <video id="my-video"
    class="video-js"
    style="width:100%; height:100%;"
    controls
    preload="auto"
    width="auto"
    height="auto"
    poster="<?php echo $production["production_photo"] ?>"
    data-setup="{}" 
       class="un729">
       <source src="<?php echo $production["videoLink"] ?>" type="video/<?php substr($production["videoLink"], -3) ?>" />
            </video>
          </div>
          <div class="v2 ps346 s440 c515">
            <p class="p1 f93">GUNNEBO BEHIND THE SCENES</p>
          </div>
          <div class="v2 ps155 s441 c516"></div>
        </div>
      </div>
    </div>
  </div>
  <div id="popup400" style="transform:translate3d(-999999px,0,0)" data-popup-type="1" class="popup ps144"
    onclick="pop.closePopup('popup400')">
    <div class="ps145">
      <div class="animm bounceIn un727 modalpopup v6 ps345 s438 c513" onclick="event.stopPropagation()">
        <div class="c514">
          <style>
            .swiper {
  width: 600px;
  height: 600px;
}
          </style>


<div class="swiper">
  <!-- Additional required wrapper -->
  <div class="swiper-wrapper">
    <!-- Slides -->

    <?php for ($i=0; $i < count($behinds); $i++) { 
      $photo = $behinds[$i];
      
      ?>

      <div class="swiper-slide">
        <img style="width:600px; height:600px;"  src="<?php echo $photo->image ?>" alt="">
      </div>      

    <?php } ?>
   
  </div>


  <!-- If we need navigation buttons -->
  <div class="swiper-button-prev"></div>
  <div class="swiper-button-next"></div>

  <!-- If we need scrollbar -->
  <div class="swiper-scrollbar"></div>
</div>
  <script>
    const swiper = new Swiper('.swiper', {
  // Optional parameters
  direction: 'horizontal',
  loop: true,



  // Navigation arrows
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },

  // And if we need scrollbar
  scrollbar: {
    el: '.swiper-scrollbar',
  },
});
  </script>

          <div class="v2 ps346 s440 c515">
            <p class="p1 f93">GUNNEBO BEHIND THE SCENES</p>
          </div>
          <div class="v2 ps155 s441 c516"></div>
        </div>
      </div>
    </div>
  </div>
  <div id="popup39" style="transform:translate3d(-999999px,0,0)" data-popup-type="1" class="popup ps347"
    onclick="pop.closePopup('popup39')">
    <div class="animm bounceInRight un731 modalpopup v5 ps34 s30 c517" onclick="event.stopPropagation()">
      <div class="c518 s31">
      </div>
      <div class="c43">
        <div class="anim fadeIn un733 v2 ps35 s32 c519">
          <a onclick="pop&&pop.closePopup('popup39');return false" style="cursor:pointer;" href="javascript:void(0)"
            class="a1"><svg viewBox="0 0 88 88" xmlns="http://www.w3.org/2000/svg" class="i1">
              <g transform="translate(-1102-49)">
                <g transform="translate(1102.16 49.1594)">
                  <g fill-rule="evenodd" style="fill:currentColor" stroke="none">
                    <polygon points="-16.1594 45.8406 103.841 45.8406 103.841 41.8406 -16.1594 41.8406"
                      transform="matrix(.7071.7071-.70711.7071 43.8406-18.1594)" />
                    <polygon points="-16.1594 45.8406 103.841 45.8406 103.841 41.8406 -16.1594 41.8406"
                      transform="matrix(.7071-.70711.7071.7071-18.1594 43.8406)" />
                  </g>
                </g>
              </g>
            </svg></a>
        </div>
        <div class="v2 ps36 s33 c520">
          <p class="p2 f5">WE ARE HERE AND READY!</p>
        </div>
        <div id="menu" class="v4 ps37 s34 c521">
        <?php 
       
       for ($i=0; $i < count($countrys); $i++) { 
        
        echo '<p class="p4 f6"><a onclick="pop.openClosePopup(\'popup2\')" style="cursor:pointer;" href="Country/'.$countrys[$i]["country"].'">'.$countrys[$i]['country'].'</a></p>';

       }

       ?>
        </div>
        <div class="v2 ps38 s35 c522"></div>
        <div id="menu" class="v2 ps39 s36 c523">
         
       <?php 
       
       for ($i=0; $i < count($countrys); $i++) { 
        
        echo '<p class="p2 f7"><a onclick="pop.openClosePopup(\'popup2\')" style="cursor:pointer;" href="Country/'.$countrys[$i]["country"].'">'.$countrys[$i]['country'].'</a></p>';

       }

       ?>
        </div>
        <div class="v2 ps40 s37 c524"></div>
        <div id="menu" class="v2 ps41 s38 c525" style="height: 260px;">
          <p class="p2 f8"><a onclick="pop.openClosePopup('popup39')" style="cursor:pointer;" href="about">About
              Us</a></p>
          <p class="p2 f8"><a onclick="pop.openClosePopup('popup39')" style="cursor:pointer;"
              href="contact">Contact</a></p>
          <p class="p2 f8"><a onclick="pop.openClosePopup('popup39')" style="cursor:pointer;"
              href="directors">Directors</a></p>
              <p class="p2 f8"><a onclick="pop.openClosePopup('popup39')" style="cursor:pointer;"
              href="clients">Clients</a></p>
        </div>
        <div class="v2 ps42 s39 c51" style="margin-top: -260px;">
          <p class="p2 f9"> <?php  echo $site_info["menu_subtitle"] ?> </p>
        </div>
        <div id="menu" class="v2 ps43 s40 c52">
          <p class="p5 f10"><a onclick="pop.openClosePopup('popup2')" style="cursor:pointer;"
          href="<?php echo $site_info["facebook"] ?>" target="_blank" rel="noopener">
              <x-svg class="s41"><svg style='fill:currentColor' height='100%' xmlns='http://www.w3.org/2000/svg'
                  viewBox='0 0 448 512'>
                  <path
                    d='M400 32H48A48 48 0 0 0 0 80v352a48 48 0 0 0 48 48h137.25V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.27c-30.81 0-40.42 19.12-40.42 38.73V256h68.78l-11 71.69h-57.78V480H400a48 48 0 0 0 48-48V80a48 48 0 0 0-48-48z' />
                </svg></x-svg>
            </a><span class="f11"> </span><span class="f10"><a onclick="pop.openClosePopup('popup2')"
                style="cursor:pointer;" href="<?php echo $site_info["instagram"] ?>" target="_blank" rel="noopener">
                <x-svg class="s41"><svg style='fill:currentColor' height='100%' xmlns='http://www.w3.org/2000/svg'
                    viewBox='0 0 448 512'>
                    <path
                      d='M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z' />
                  </svg></x-svg>
              </a></span><span class="f11"> </span><span class="f12"><a onclick="pop.openClosePopup('popup2')"
                style="cursor:pointer;" href="<?php echo $site_info["twitter"] ?>" target="_blank" rel="noopener">
                <x-svg class="s42"><svg style='fill:currentColor' height='100%' xmlns='http://www.w3.org/2000/svg'
                    viewBox='0 0 512 512'>
                    <path
                      d='M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z' />
                  </svg></x-svg>
              </a></span></p>
        </div>
      </div>
    </div>
  </div>
  <div id="popup40" style="transform:translate3d(-999999px,0,0)" data-popup-type="2" data-popup-group="0" class="ps348">
    <div onclick="event.stopPropagation()" class="popup v6 ps45 s43 c528">
      <div class="v2 ps46 s44 c119">
        <a onclick="pop.openClosePopup('popup40')" style="cursor:pointer;" href="/" class="a1"><img
            src="images/fps-logo-white.svg" class="i2"></a>
      </div>
    </div>
  </div>
  <div id="popup41" style="transform:translate3d(-999999px,0,0)" data-popup-type="2" data-popup-group="3" class="ps89">
    <div onclick="event.stopPropagation()" class="popup v5 ps48 s45 c529">
      <div class="c56">
        <div class="v2 ps49 s46 c129">
          <a onclick="pop&&pop.openPopup('popup39');return false" style="cursor:pointer;" href="javascript:void(0)"
            class="a1"><svg viewBox="0 0 120 78" xmlns="http://www.w3.org/2000/svg" class="i3">
              <g transform="translate(-1248-54)">
                <g transform="translate(1248 54)">
                  <g fill-rule="evenodd" style="fill:currentColor" stroke="none">
                    <polygon points="0 4 120 4 120 0 0 0" />
                    <polygon points="0 41 120 41 120 37 0 37" />
                    <polygon points="0 78 120 78 120 74 0 74" />
                  </g>
                </g>
              </g>
            </svg></a>
        </div>
      </div>
    </div>
  </div>
  <div class="c132">
  </div>
  <script>var p = document.createElement("P"); p.innerHTML = "&nbsp;", p.style.cssText = "position:fixed;visible:hidden;font-size:100px;zoom:1", document.body.appendChild(p); var rsz = function (e) { return function () { var r = parseInt(1e7 / parseFloat(window.getComputedStyle(e).getPropertyValue("font-size")) + .5) / 1e5, n = document.body; r != n.style.getPropertyValue("--f") && n.style.setProperty("--f", r) } }(p); if ("ResizeObserver" in window) { var ro = new ResizeObserver(rsz); ro.observe(p) } else if ("requestAnimationFrame" in window) { var raf = function () { rsz(), requestAnimationFrame(raf) }; requestAnimationFrame(raf) } else setInterval(rsz, 100);</script>

  <script>dpth = "/"</script>
  <script type="text/javascript" src="js/popup.bfc084.js"></script>
  <script type="text/javascript" src="js/woolite.bfc084.js"></script>
  <script type="text/javascript" src="js/jquery.bfc084.js"></script>
  <script type="text/javascript" src="js/stickyfill.bfc084.js"></script>
  <script type="text/javascript" src="js/gunnebo.54ee8e.js"></script>
  <script src="https://vjs.zencdn.net/7.20.1/video.min.js"></script>
</body>

</html>