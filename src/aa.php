
<?php 

include "classes/MainClass.php";
$veritabani = new MainClass();
$countrys = $veritabani->getCountrys();
$country = $veritabani->getCountry($_GET["country"]);

if (isset($_GET)) {
  $productions = $veritabani->getProductionsFiltre($_GET["filtre"],$country["country_id"]);
$locations = $veritabani->getLocationsFiltre($_GET["filtre"],$country["country_id"]);


}else{
  $productions = $veritabani->getProduction($country["country_id"]); 
  $locations = $veritabani->getLocations($country["country_id"]);
}
$arr1 = [];
$arr2 = [];
foreach ($productions as $production) {
  $arr1[] = array(
    "title"=>$production["production_title"],
    "image"=> $production["production_photo"],
    "isVideo"=>$production["isVideo"],
    "video"=>$production["videoLink"],
  );
}
foreach ($locations as $location) {
  $arr2[] = array(
    "title"=>$location["location"],
    "image"=> $location["location_photo"],
    "isVideo"=>0,
    "video"=>0
  );   
}
$arr = array_merge($arr1,$arr2);
var_dump($arr);

?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>Productions</title>
  <meta name="referrer" content="same-origin">
  <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
  <base href="http:/projects.bluemind.agency:8020/">
  <style>
    .anim {
      visibility: hidden
    }
  </style>
  <script>!function () { var A = new Image; A.onload = A.onerror = function () { 1 != A.height && document.body.classList.remove("webp") }, A.src = "data:image/webp;base64,UklGRiQAAABXRUJQVlA4IBgAAAAwAQCdASoBAAEAD8D+JaQAA3AA/ua1AAA" }();
  </script>
  <link rel="stylesheet" href="css/site.54ee8e.css" type="text/css">
</head>

<body class="webp" id="b1">
  <div class="v8 ps278 s340 c435">
    <div class="ps279">
    </div>
    <div class="v2 ps1 s341 c436">
      <div class="bd c437">
        <div class="c438 s31">
        </div>
      </div>
      <div class="ps280 v8 s342">
        <div id="other" class="v3 ps281 s343 c439">
          <p class="p6 f24"><a href="turkey.html">Turkey</a></p>
        </div>
      </div>
      <div class="v3 ps282 s344 c440">
        <div class="ps283 v8 s345">
          <div class="v3 ps284 s346 c441">
            <div class="v3 ps285 s347 w2">
              <div id="commercials-altmenu" class="v3 ps286 s348 c442">
                <p class="p6 f63">
                  <x-svg class="s349"><svg style='fill:currentColor' height='100%' xmlns='http://www.w3.org/2000/svg'
                      viewBox='0 0 496 512'>
                      <path
                        d='M248 8C111.03 8 0 119.03 0 256s111.03 248 248 248 248-111.03 248-248S384.97 8 248 8zm82.29 357.6c-3.9 3.88-7.99 7.95-11.31 11.28-2.99 3-5.1 6.7-6.17 10.71-1.51 5.66-2.73 11.38-4.77 16.87l-17.39 46.85c-13.76 3-28 4.69-42.65 4.69v-27.38c1.69-12.62-7.64-36.26-22.63-51.25-6-6-9.37-14.14-9.37-22.63v-32.01c0-11.64-6.27-22.34-16.46-27.97-14.37-7.95-34.81-19.06-48.81-26.11-11.48-5.78-22.1-13.14-31.65-21.75l-.8-.72a114.792 114.792 0 0 1-18.06-20.74c-9.38-13.77-24.66-36.42-34.59-51.14 20.47-45.5 57.36-82.04 103.2-101.89l24.01 12.01C203.48 89.74 216 82.01 216 70.11v-11.3c7.99-1.29 16.12-2.11 24.39-2.42l28.3 28.3c6.25 6.25 6.25 16.38 0 22.63L264 112l-10.34 10.34c-3.12 3.12-3.12 8.19 0 11.31l4.69 4.69c3.12 3.12 3.12 8.19 0 11.31l-8 8a8.008 8.008 0 0 1-5.66 2.34h-8.99c-2.08 0-4.08.81-5.58 2.27l-9.92 9.65a8.008 8.008 0 0 0-1.58 9.31l15.59 31.19c2.66 5.32-1.21 11.58-7.15 11.58h-5.64c-1.93 0-3.79-.7-5.24-1.96l-9.28-8.06a16.017 16.017 0 0 0-15.55-3.1l-31.17 10.39a11.95 11.95 0 0 0-8.17 11.34c0 4.53 2.56 8.66 6.61 10.69l11.08 5.54c9.41 4.71 19.79 7.16 30.31 7.16s22.59 27.29 32 32h66.75c8.49 0 16.62 3.37 22.63 9.37l13.69 13.69a30.503 30.503 0 0 1 8.93 21.57 46.536 46.536 0 0 1-13.72 32.98zM417 274.25c-5.79-1.45-10.84-5-14.15-9.97l-17.98-26.97a23.97 23.97 0 0 1 0-26.62l19.59-29.38c2.32-3.47 5.5-6.29 9.24-8.15l12.98-6.49C440.2 193.59 448 223.87 448 256c0 8.67-.74 17.16-1.82 25.54L417 274.25z' />
                    </svg></x-svg>
                </p>
              </div>
              <div class="v3 ps287 s350 c443">
                <div class="v2 ps2 s351 c1">
                  <div id="locations-gallery" class="v2 ps2 s352 c103">
                    <div class="v2 ps2 s352 c1">
                      <div class="v2 ps2 s352 w1">
                        <div class="v2 ps2 s352 c444">
                          <a href="#" class="f64 btn9 v7 s353">FEATURE FILMS &amp; TV SERIES</a>
                        </div>
                        <div class="v2 ps288 s354 c445"></div>
                      </div>
                    </div>
                  </div>
                  <div id="locations-gallery" class="v2 ps289 s355 c339">
                    <div class="v2 ps2 s355 c1">
                      <div class="v2 ps2 s355 w1">
                        <div class="v2 ps2 s355 c338">
                          <a href="#" class="f65 btn10 v7 s356">TV COMMERCIALS</a>
                        </div>
                        <div class="v2 ps288 s357 c446"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="commercials-altmenu" class="v3 ps290 s358 c447">
              <p class="p6 f66">INTERNATIONAL PRODUCTIONS</p>
            </div>
            <div class="v3 ps291 s359 c448">
              <div class="v2 ps292 s360 c1">
                <div id="commercials-altmenu" class="v2 ps293 s361 c449">
                  <p class="p2 f67">
                    <x-svg class="s362"><svg style='fill:currentColor' height='100%' xmlns='http://www.w3.org/2000/svg'
                        viewBox='0 0 384 512'>
                        <path
                          d='M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z' />
                      </svg></x-svg>
                  </p>
                </div>
                <div id="commercials-altmenu" class="v2 ps294 s363 c450">
                  <p class="p2 f68">LOCAL PRODUCTIONS</p>
                </div>
              </div>
              <div class="v2 ps295 s364 c74">
                <div class="v2 ps2 s364 c1">
                  <div id="locations-gallery" class="v2 ps2 s365 c451">
                    <div class="v2 ps2 s365 c1">
                      <div class="v2 ps2 s365 w1">
                        <div class="v2 ps2 s365 c89">
                          <a href="#" class="f69 btn11 v7 s366">FILMS</a>
                        </div>
                        <div class="v2 ps288 s367 c452"></div>
                      </div>
                    </div>
                  </div>
                  <div id="locations-gallery" class="v2 ps296 s368 c453">
                    <div class="v2 ps2 s368 c1">
                      <div class="v2 ps2 s368 w1">
                        <div class="v2 ps2 s368 c454">
                          <a href="#" class="f69 btn12 v7 s369">TV COMMERCIALS</a>
                        </div>
                        <div class="v2 ps288 s370 c455"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="ps297 v1 s146">
    <div class="v2 ps3 s3 c196">
      <div class="wrapper8">
        <style>
          ::selection {
            color: white;
            background: #ff7200;
          }

          #menu a {
            transition: all 0.5s ease;
          }

          #menu a:hover {
            transition: all 0.5s ease;
            color: #ffffff;
          }

          #earth a {
            transition: all 0.5s ease;
          }

          #earth a:hover {
            transition: all 0.5s ease;
            color: #FF6700;
          }

          #hero a {
            transition: all 0.5s ease;
          }

          #hero a:hover {
            transition: all 0.5s ease;
            color: #ffffff;
          }

          #locations-folders {
            transition: all 0.5s ease;
            opacity: 0.8;
          }

          #locations-folders:hover {
            transition: all 0.5s ease;
            transform: translateY(-10px) scale(1.1);
            opacity: 1;
          }

          #overlay {
            transition: all 0.5s ease;
            opacity: 0;
            transform: scale(0.8);
          }

          #overlay:hover {
            transition: all 0.5s ease;
            opacity: 0.7;
            transform: scale(1);
          }

          #locations-gallery {
            transition: all 0.5s ease;
            opacity: 1;
          }

          #locations-gallery:hover {
            transition: all 0.5s ease;
            transform: translateY(-5px) scale(1);
            opacity: 1;
          }

          #locations-gallery a:hover {
            color: #FF6700;
          }

          #locations-link {
            opacity: 0;
          }

          #locations-link:hover {
            opacity: 1;
          }

          #other a:hover {
            color: #FF6700;
            transition: all 0.5s ease;
          }

          #product p {
            transition: all 0.5s ease;
          }

          #product p:hover {
            transition: all 0.5s ease;
            color: #ffffff;
          }

          #bigbaslik a {
            transition: all 0.5s ease;
          }

          #bigbaslik a:hover {
            color: #FF6700;
            transition: all 0.5s ease;
          }

          #commercials-altmenu a {
            transition: all 0.5s ease;
          }

          #commercials-altmenu a:hover {
            transition: all 0.5s ease;
            color: #FF6700;
          }

          #commercial-link a {
            transition: all 0.5s ease;
          }

          #commercial-link a:hover {
            transition: all 0.5s ease;
            color: #FF6700;
          }

          #about {
            transition: all 0.5s ease;
            opacity: 0.4;
          }

          #about:hover {
            transition: all 0.5s ease;
            transform: translateY(-10px);
            opacity: 1;
          }

          #bts {
            transition: all 0.5s ease;
            transform: scale(1);
          }

          #bts:hover {
            transition: all 0.5s ease;
            transform: scale(1.1);
          }

          #directors {
            transition: all 0.5s ease;
            opacity: 0.5;
          }

          #directors:hover {
            transition: all 0.5s ease;
            transform: translateY(-10px);
            opacity: 1;
          }
        </style>
      </div>
    </div>
  </div>
  <div class="v4 ps298 s371 c456">
    <div class="v2 ps1 s341 c457">
      <div class="bd c437">
        <div class="c458 s31">
        </div>
      </div>
      <div class="ps299 v9 s372">
        <div id="other" class="v4 ps300 s373 c459">
          <p class="p4 f70"><a href="turkey.html">Turkey</a></p>
        </div>
      </div>
      <div class="v4 ps301 s374 c460">
        <div class="ps302 v9 s375">
          <div class="v4 ps300 s376 c461">
            <div class="v4 ps122 s377 w3">
              <div id="commercials-altmenu" class="v4 ps122 s378 c462">
                <p class="p4 f71">
                  <x-svg class="s379"><svg style='fill:currentColor' height='100%' xmlns='http://www.w3.org/2000/svg'
                      viewBox='0 0 496 512'>
                      <path
                        d='M248 8C111.03 8 0 119.03 0 256s111.03 248 248 248 248-111.03 248-248S384.97 8 248 8zm82.29 357.6c-3.9 3.88-7.99 7.95-11.31 11.28-2.99 3-5.1 6.7-6.17 10.71-1.51 5.66-2.73 11.38-4.77 16.87l-17.39 46.85c-13.76 3-28 4.69-42.65 4.69v-27.38c1.69-12.62-7.64-36.26-22.63-51.25-6-6-9.37-14.14-9.37-22.63v-32.01c0-11.64-6.27-22.34-16.46-27.97-14.37-7.95-34.81-19.06-48.81-26.11-11.48-5.78-22.1-13.14-31.65-21.75l-.8-.72a114.792 114.792 0 0 1-18.06-20.74c-9.38-13.77-24.66-36.42-34.59-51.14 20.47-45.5 57.36-82.04 103.2-101.89l24.01 12.01C203.48 89.74 216 82.01 216 70.11v-11.3c7.99-1.29 16.12-2.11 24.39-2.42l28.3 28.3c6.25 6.25 6.25 16.38 0 22.63L264 112l-10.34 10.34c-3.12 3.12-3.12 8.19 0 11.31l4.69 4.69c3.12 3.12 3.12 8.19 0 11.31l-8 8a8.008 8.008 0 0 1-5.66 2.34h-8.99c-2.08 0-4.08.81-5.58 2.27l-9.92 9.65a8.008 8.008 0 0 0-1.58 9.31l15.59 31.19c2.66 5.32-1.21 11.58-7.15 11.58h-5.64c-1.93 0-3.79-.7-5.24-1.96l-9.28-8.06a16.017 16.017 0 0 0-15.55-3.1l-31.17 10.39a11.95 11.95 0 0 0-8.17 11.34c0 4.53 2.56 8.66 6.61 10.69l11.08 5.54c9.41 4.71 19.79 7.16 30.31 7.16s22.59 27.29 32 32h66.75c8.49 0 16.62 3.37 22.63 9.37l13.69 13.69a30.503 30.503 0 0 1 8.93 21.57 46.536 46.536 0 0 1-13.72 32.98zM417 274.25c-5.79-1.45-10.84-5-14.15-9.97l-17.98-26.97a23.97 23.97 0 0 1 0-26.62l19.59-29.38c2.32-3.47 5.5-6.29 9.24-8.15l12.98-6.49C440.2 193.59 448 223.87 448 256c0 8.67-.74 17.16-1.82 25.54L417 274.25z' />
                    </svg></x-svg>
                </p>
              </div>
              <div class="v4 ps303 s380 c463">
                <div class="v2 ps2 s351 c1">
                  <div id="locations-gallery" class="v2 ps2 s352 c464">
                    <div class="v2 ps2 s352 c1">
                      <div class="v2 ps2 s352 w1">
                        <div class="v2 ps2 s352 c465">
                          <a href="#" class="f64 btn13 v7 s353">FEATURE FILMS &amp; TV SERIES</a>
                        </div>
                        <div class="v2 ps288 s354 c466"></div>
                      </div>
                    </div>
                  </div>
                  <div id="locations-gallery" class="v2 ps289 s355 c284">
                    <div class="v2 ps2 s355 c1">
                      <div class="v2 ps2 s355 w1">
                        <div class="v2 ps2 s355 c467">
                          <a href="#" class="f65 btn14 v7 s356">TV COMMERCIALS</a>
                        </div>
                        <div class="v2 ps288 s357 c468"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="commercials-altmenu" class="v4 ps304 s381 c469">
              <p class="p4 f72">INTERNATIONAL PRODUCTIONS</p>
            </div>
            <div class="v4 ps305 s382 c470">
              <div class="v2 ps292 s360 c1">
                <div id="commercials-altmenu" class="v2 ps293 s361 c449">
                  <p class="p2 f67">
                    <x-svg class="s362"><svg style='fill:currentColor' height='100%' xmlns='http://www.w3.org/2000/svg'
                        viewBox='0 0 384 512'>
                        <path
                          d='M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z' />
                      </svg></x-svg>
                  </p>
                </div>
                <div id="commercials-altmenu" class="v2 ps294 s363 c450">
                  <p class="p2 f68">LOCAL PRODUCTIONS</p>
                </div>
              </div>
              <div class="v2 ps295 s364 c350">
                <div class="v2 ps2 s364 c1">
                  <div id="locations-gallery" class="v2 ps2 s365 c351">
                    <div class="v2 ps2 s365 c1">
                      <div class="v2 ps2 s365 w1">
                        <div class="v2 ps2 s365 c471">
                          <a href="#" class="f69 btn15 v7 s366">FILMS</a>
                        </div>
                        <div class="v2 ps288 s367 c472"></div>
                      </div>
                    </div>
                  </div>
                  <div id="locations-gallery" class="v2 ps296 s368 c353">
                    <div class="v2 ps2 s368 c1">
                      <div class="v2 ps2 s368 w1">
                        <div class="v2 ps2 s368 c352">
                          <a href="#" class="f69 btn16 v7 s369">TV COMMERCIALS</a>
                        </div>
                        <div class="v2 ps288 s370 c473"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="ps306 v1 s383">
    <div class="v2 ps264 s384 c4">
      <div class="v2 ps2 s384 c1">
      <?php   
          
          for ($i=0; $i < count($arr); $i++) { 
            $elem = $arr[$i];
           
          
          
          
          ?>
        <div class="v2 ps2 s385 c474">
      
          <div id="locations-gallery"  class="v2 ps2 s386 c64">
            <div class="v2 ps2 s386 c1">
              <div class="v2 ps2 s386 w1">
                <div class="v2 ps2 s387 c421">
                  <a href="gunnebo.html" class="a1">
                  <?php if ($elem->isVideo == 1) { ?>
                    <iframe src="<?php echo $elem["video"] ?>" frameborder="0"></iframe>
                  <?php }else { 
                    ?> 
                    <picture class="i5">
                      
                        <img src="<?php echo $elem["image"] ?>"class="un658 i41">
                    </picture>
                    <?php } ?>       
                  </a>
                </div>
                <div class="v2 ps307 s388 c417">
                 
                  <a href="#" class="f73 btn17 v7 s389"> <p> </p>
                    <p> </p>
                    <p> </p>
                    <p> </p>
                    <p> </p>
                    <p> </p>
                    <p> </p>
                    <p><?php echo $elem["title"] ?></p>
                  </a>
                </div>
              </div>
            </div>
          </div>
           
        

        </div>
        <?php } ?>
      </div>
    </div>
  </div>
  <div class="v2 ps319 s168 c478"></div>
  <div id="popup32" style="transform:translate3d(-999999px,0,0)" data-popup-type="2" data-popup-group="7" class="ps143">
    <div onclick="event.stopPropagation()" class="popup v5 ps29 s26 c479">
      <div class="v4 ps30 s27 c38">
        <div class="v4 ps31 s28 c480"></div>
        <div class="v4 ps32 s29 c481"></div>
      </div>
    </div>
  </div>
  <div id="popup33" style="transform:translate3d(-999999px,0,0)" data-popup-type="1" class="popup ps156"
    onclick="pop.closePopup('popup33')">
    <div class="animm bounceInRight un667 modalpopup v5 ps34 s30 c482" onclick="event.stopPropagation()">
      <div class="c483 s31">
      </div>
      <div class="c43">
        <div class="anim fadeIn un669 v2 ps35 s32 c129">
          <a onclick="pop&&pop.closePopup('popup33');return false" style="cursor:pointer;" href="javascript:void(0)"
            class="a1"><svg viewBox="0 0 88 88" xmlns="http://www.w3.org/2000/svg" class="i1">
              <g transform="translate(-1102-49)">
                <g transform="translate(1102.16 49.1594)">
                  <g fill-rule="evenodd" style="fill:currentColor" stroke="none">
                    <polygon points="-16.1594 45.8406 103.841 45.8406 103.841 41.8406 -16.1594 41.8406"
                      transform="matrix(.7071.7071-.70711.7071 43.8406-18.1594)" />
                    <polygon points="-16.1594 45.8406 103.841 45.8406 103.841 41.8406 -16.1594 41.8406"
                      transform="matrix(.7071-.70711.7071.7071-18.1594 43.8406)" />
                  </g>
                </g>
              </g>
            </svg></a>
        </div>
        <div class="v2 ps36 s33 c226">
          <p class="p2 f5">WE ARE HERE AND READY!</p>
        </div>
        <div id="menu" class="v4 ps37 s34 c227">
          <p class="p4 f6"><a onclick="pop.openClosePopup('popup33')" style="cursor:pointer;"
              href="turkey.html">Azerbaijan</a></p>
          <p class="p4 f6"><a onclick="pop.openClosePopup('popup33')" style="cursor:pointer;"
              href="turkey.html">Georgia</a></p>
        </div>
        <div class="v2 ps38 s35 c484"></div>
        <div id="menu" class="v2 ps39 s36 c229">
          <p class="p2 f7"><a onclick="pop.openClosePopup('popup33')" style="cursor:pointer;"
              href="turkey.html">Turkey</a></p>
          <p class="p2 f7"><a onclick="pop.openClosePopup('popup33')" style="cursor:pointer;" href="turkey.html">New
              York</a></p>
          <p class="p2 f7"><a onclick="pop.openClosePopup('popup33')" style="cursor:pointer;"
              href="turkey.html">Azerbaijan</a></p>
          <p class="p2 f7"><a onclick="pop.openClosePopup('popup33')" style="cursor:pointer;"
              href="turkey.html">Georgia</a></p>
        </div>
        <div class="v2 ps40 s37 c485"></div>
        <div id="menu" class="v2 ps41 s38 c231">
          <p class="p2 f8"><a onclick="pop.openClosePopup('popup33')" style="cursor:pointer;" href="about-us.html">About
              Us</a></p>
          <p class="p2 f8"><a onclick="pop.openClosePopup('popup33')" style="cursor:pointer;"
              href="contact.html">Contact</a></p>
          <p class="p2 f8"><a onclick="pop.openClosePopup('popup33')" style="cursor:pointer;"
              href="directors.html">Directors</a></p>
        </div>
        <div class="v2 ps42 s39 c232">
          <p class="p2 f9">FPS Productions is a full service production company headquartered in Istanbul, Turkey.
            Established 12 years ago, FPS prides itself in working with some of the top agencies and production
            companies across the world. With a multilingual in house staff and years of combined international
            experience, we have serviced productions from a vast array of countries such as UK, Germany, USA, India,
            Poland, etc.&nbsp;</p>
        </div>
        <div id="menu" class="v2 ps43 s40 c233">
          <p class="p5 f10"><a onclick="pop.openClosePopup('popup33')" style="cursor:pointer;"
              href="https://www.facebook.com/FPSAzerbaijan/" target="_blank" rel="noopener">
              <x-svg class="s41"><svg style='fill:currentColor' height='100%' xmlns='http://www.w3.org/2000/svg'
                  viewBox='0 0 448 512'>
                  <path
                    d='M400 32H48A48 48 0 0 0 0 80v352a48 48 0 0 0 48 48h137.25V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.27c-30.81 0-40.42 19.12-40.42 38.73V256h68.78l-11 71.69h-57.78V480H400a48 48 0 0 0 48-48V80a48 48 0 0 0-48-48z' />
                </svg></x-svg>
            </a><span class="f11"> </span><span class="f10"><a onclick="pop.openClosePopup('popup33')"
                style="cursor:pointer;" href="https://www.instagram.com/fpsprod/" target="_blank" rel="noopener">
                <x-svg class="s41"><svg style='fill:currentColor' height='100%' xmlns='http://www.w3.org/2000/svg'
                    viewBox='0 0 448 512'>
                    <path
                      d='M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z' />
                  </svg></x-svg>
              </a></span><span class="f11"> </span><span class="f12"><a onclick="pop.openClosePopup('popup33')"
                style="cursor:pointer;" href="https://twitter.com/fpsazerbaijan" target="_blank" rel="noopener">
                <x-svg class="s42"><svg style='fill:currentColor' height='100%' xmlns='http://www.w3.org/2000/svg'
                    viewBox='0 0 512 512'>
                    <path
                      d='M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z' />
                  </svg></x-svg>
              </a></span></p>
        </div>
      </div>
    </div>
  </div>
  <div id="popup34" style="transform:translate3d(-999999px,0,0)" data-popup-type="2" data-popup-group="0" class="ps90">
    <div onclick="event.stopPropagation()" class="popup v6 ps45 s43 c486">
      <div class="v2 ps46 s44 c131">
        <a onclick="pop.openClosePopup('popup34')" style="cursor:pointer;" href="index.html" class="a1"><img
            src="images/fps-logo-white.svg" class="i2"></a>
      </div>
    </div>
  </div>
  <div id="popup35" style="transform:translate3d(-999999px,0,0)" data-popup-type="2" data-popup-group="3" class="ps157">
    <div onclick="event.stopPropagation()" class="popup v5 ps48 s45 c487">
      <div class="c56">
        <div class="v2 ps49 s46 c236">
          <a onclick="pop&&pop.openPopup('popup33');return false" style="cursor:pointer;" href="javascript:void(0)"
            class="a1"><svg viewBox="0 0 120 78" xmlns="http://www.w3.org/2000/svg" class="i3">
              <g transform="translate(-1248-54)">
                <g transform="translate(1248 54)">
                  <g fill-rule="evenodd" style="fill:currentColor" stroke="none">
                    <polygon points="0 4 120 4 120 0 0 0" />
                    <polygon points="0 41 120 41 120 37 0 37" />
                    <polygon points="0 78 120 78 120 74 0 74" />
                  </g>
                </g>
              </g>
            </svg></a>
        </div>
      </div>
    </div>
  </div>
  <div class="c132">
  </div>
  <script>var p = document.createElement("P"); p.innerHTML = "&nbsp;", p.style.cssText = "position:fixed;visible:hidden;font-size:100px;zoom:1", document.body.appendChild(p); var rsz = function (e) { return function () { var r = parseInt(1e7 / parseFloat(window.getComputedStyle(e).getPropertyValue("font-size")) + .5) / 1e5, n = document.body; r != n.style.getPropertyValue("--f") && n.style.setProperty("--f", r) } }(p); if ("ResizeObserver" in window) { var ro = new ResizeObserver(rsz); ro.observe(p) } else if ("requestAnimationFrame" in window) { var raf = function () { rsz(), requestAnimationFrame(raf) }; requestAnimationFrame(raf) } else setInterval(rsz, 100);</script>

  <script>dpth = "/"</script>
  <script type="text/javascript" src="js/popup.bfc084.js"></script>
  <script type="text/javascript" src="js/woolite.bfc084.js"></script>
  <script type="text/javascript" src="js/jquery.bfc084.js"></script>
  <script type="text/javascript" src="js/stickyfill.bfc084.js"></script>
  <script type="text/javascript" src="js/turkey-productions.54ee8e.js"></script>
</body>

</html>
